/* A Bison parser, made by GNU Bison 3.0.4.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "3.0.4"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* Copy the first part of user declarations.  */
#line 1 "gram.y" /* yacc.c:339  */

#include <stdlib.h>

#include "sc.h"
#include "cmds.h"
#include "interp.h"
#include "macros.h"
#include "sort.h"
#include "filter.h"
#include "maps.h"
#include "marks.h"
#include "xmalloc.h" // for scxfree
#include "hide_show.h"
#include "cmds_normal.h"
#include "conf.h"
#include "pipe.h"
#include "main.h"
#include "tui.h"
#include "freeze.h"
#include "undo.h"
#include "dep_graph.h"
#include "utils/dictionary.h"
#include "trigger.h"
#include "shift.h"
#include "clipboard.h"
#include "plot.h"
#include "subtotal.h"

void yyerror(char *err);               // error routine for yacc (gram.y)
int yylex();

#ifdef USELOCALE
#include <locale.h>
#endif

#ifndef MSDOS
#include <unistd.h>
#endif

#define ENULL (struct enode *) 0

#line 108 "y.tab.c" /* yacc.c:339  */

# ifndef YY_NULLPTR
#  if defined __cplusplus && 201103L <= __cplusplus
#   define YY_NULLPTR nullptr
#  else
#   define YY_NULLPTR 0
#  endif
# endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* In a future release of Bison, this section will be replaced
   by #include "y.tab.h".  */
#ifndef YY_YY_Y_TAB_H_INCLUDED
# define YY_YY_Y_TAB_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    STRING = 258,
    COL = 259,
    NUMBER = 260,
    FNUMBER = 261,
    RANGE = 262,
    VAR = 263,
    WORD = 264,
    MAPWORD = 265,
    PLUGIN = 266,
    S_SHOW = 267,
    S_HIDE = 268,
    S_SHOWROW = 269,
    S_HIDEROW = 270,
    S_SHOWCOL = 271,
    S_HIDECOL = 272,
    S_FREEZE = 273,
    S_UNFREEZE = 274,
    S_MARK = 275,
    S_AUTOJUS = 276,
    S_PAD = 277,
    S_DATEFMT = 278,
    S_SUBTOTAL = 279,
    S_RSUBTOTAL = 280,
    S_FORMAT = 281,
    S_FMT = 282,
    S_LET = 283,
    S_LABEL = 284,
    S_LEFTSTRING = 285,
    S_RIGHTSTRING = 286,
    S_LEFTJUSTIFY = 287,
    S_RIGHTJUSTIFY = 288,
    S_CENTER = 289,
    S_SORT = 290,
    S_FILTERON = 291,
    S_GOTO = 292,
    S_CCOPY = 293,
    S_CPASTE = 294,
    S_PLOT = 295,
    S_LOCK = 296,
    S_UNLOCK = 297,
    S_DEFINE = 298,
    S_UNDEFINE = 299,
    S_DETAIL = 300,
    S_EVAL = 301,
    S_SEVAL = 302,
    S_ERROR = 303,
    S_FILL = 304,
    S_SHIFT = 305,
    S_GETNUM = 306,
    S_GETSTRING = 307,
    S_GETEXP = 308,
    S_GETFMT = 309,
    S_GETFORMAT = 310,
    S_RECALC = 311,
    S_QUIT = 312,
    S_REBUILD_GRAPH = 313,
    S_PRINT_GRAPH = 314,
    S_SYNCREFS = 315,
    S_REDO = 316,
    S_UNDO = 317,
    S_IMAP = 318,
    S_NMAP = 319,
    S_INOREMAP = 320,
    S_NNOREMAP = 321,
    S_NUNMAP = 322,
    S_IUNMAP = 323,
    S_COLOR = 324,
    S_CELLCOLOR = 325,
    S_UNFORMAT = 326,
    S_REDEFINE_COLOR = 327,
    S_SET = 328,
    S_FCOPY = 329,
    S_FSUM = 330,
    S_TRIGGER = 331,
    S_UNTRIGGER = 332,
    K_AUTOBACKUP = 333,
    K_NOAUTOBACKUP = 334,
    K_AUTOCALC = 335,
    K_NOAUTOCALC = 336,
    K_DEBUG = 337,
    K_NODEBUG = 338,
    K_TRG = 339,
    K_NOTRG = 340,
    K_EXTERNAL_FUNCTIONS = 341,
    K_NOEXTERNAL_FUNCTIONS = 342,
    K_HALF_PAGE_SCROLL = 343,
    K_NOHALF_PAGE_SCROLL = 344,
    K_NOCURSES = 345,
    K_CURSES = 346,
    K_NUMERIC = 347,
    K_NONUMERIC = 348,
    K_NUMERIC_DECIMAL = 349,
    K_NONUMERIC_DECIMAL = 350,
    K_NUMERIC_ZERO = 351,
    K_NONUMERIC_ZERO = 352,
    K_OVERLAP = 353,
    K_NOOVERLAP = 354,
    K_QUIT_AFTERLOAD = 355,
    K_NOQUIT_AFTERLOAD = 356,
    K_XLSX_READFORMULAS = 357,
    K_NOXLSX_READFORMULAS = 358,
    K_DEFAULT_COPY_TO_CLIPBOARD_CMD = 359,
    K_DEFAULT_PASTE_FROM_CLIPBOARD_CMD = 360,
    K_COPY_TO_CLIPBOARD_DELIMITED_TAB = 361,
    K_NOCOPY_TO_CLIPBOARD_DELIMITED_TAB = 362,
    K_IGNORECASE = 363,
    K_NOIGNORECASE = 364,
    K_TM_GMTOFF = 365,
    K_NEWLINE_ACTION = 366,
    K_ERROR = 367,
    K_INVALID = 368,
    K_FIXED = 369,
    K_SUM = 370,
    K_PROD = 371,
    K_AVG = 372,
    K_STDDEV = 373,
    K_COUNT = 374,
    K_ROWS = 375,
    K_COLS = 376,
    K_ABS = 377,
    K_FROW = 378,
    K_FCOL = 379,
    K_ACOS = 380,
    K_ASIN = 381,
    K_ATAN = 382,
    K_ATAN2 = 383,
    K_CEIL = 384,
    K_COS = 385,
    K_EXP = 386,
    K_FABS = 387,
    K_FLOOR = 388,
    K_HYPOT = 389,
    K_LN = 390,
    K_LOG = 391,
    K_PI = 392,
    K_POW = 393,
    K_SIN = 394,
    K_SQRT = 395,
    K_TAN = 396,
    K_DTR = 397,
    K_RTD = 398,
    K_MAX = 399,
    K_MIN = 400,
    K_RND = 401,
    K_ROUND = 402,
    K_IF = 403,
    K_PV = 404,
    K_FV = 405,
    K_PMT = 406,
    K_HOUR = 407,
    K_MINUTE = 408,
    K_SECOND = 409,
    K_MONTH = 410,
    K_DAY = 411,
    K_YEAR = 412,
    K_NOW = 413,
    K_DATE = 414,
    K_DTS = 415,
    K_TTS = 416,
    K_FMT = 417,
    K_REPLACE = 418,
    K_SUBSTR = 419,
    K_UPPER = 420,
    K_LOWER = 421,
    K_CAPITAL = 422,
    K_STON = 423,
    K_SLEN = 424,
    K_EQS = 425,
    K_EXT = 426,
    K_LUA = 427,
    K_NVAL = 428,
    K_SVAL = 429,
    K_LOOKUP = 430,
    K_HLOOKUP = 431,
    K_VLOOKUP = 432,
    K_INDEX = 433,
    K_STINDEX = 434,
    K_TBLSTYLE = 435,
    K_TBL = 436,
    K_LATEX = 437,
    K_SLATEX = 438,
    K_TEX = 439,
    K_FRAME = 440,
    K_RNDTOEVEN = 441,
    K_FILENAME = 442,
    K_MYROW = 443,
    K_MYCOL = 444,
    K_LASTROW = 445,
    K_LASTCOL = 446,
    K_COLTOA = 447,
    K_CRACTION = 448,
    K_CRROW = 449,
    K_CRCOL = 450,
    K_ROWLIMIT = 451,
    K_COLLIMIT = 452,
    K_PAGESIZE = 453,
    K_ERR = 454,
    K_REF = 455,
    K_LOCALE = 456,
    K_SET8BIT = 457,
    K_ASCII = 458,
    K_CHR = 459
  };
#endif
/* Tokens.  */
#define STRING 258
#define COL 259
#define NUMBER 260
#define FNUMBER 261
#define RANGE 262
#define VAR 263
#define WORD 264
#define MAPWORD 265
#define PLUGIN 266
#define S_SHOW 267
#define S_HIDE 268
#define S_SHOWROW 269
#define S_HIDEROW 270
#define S_SHOWCOL 271
#define S_HIDECOL 272
#define S_FREEZE 273
#define S_UNFREEZE 274
#define S_MARK 275
#define S_AUTOJUS 276
#define S_PAD 277
#define S_DATEFMT 278
#define S_SUBTOTAL 279
#define S_RSUBTOTAL 280
#define S_FORMAT 281
#define S_FMT 282
#define S_LET 283
#define S_LABEL 284
#define S_LEFTSTRING 285
#define S_RIGHTSTRING 286
#define S_LEFTJUSTIFY 287
#define S_RIGHTJUSTIFY 288
#define S_CENTER 289
#define S_SORT 290
#define S_FILTERON 291
#define S_GOTO 292
#define S_CCOPY 293
#define S_CPASTE 294
#define S_PLOT 295
#define S_LOCK 296
#define S_UNLOCK 297
#define S_DEFINE 298
#define S_UNDEFINE 299
#define S_DETAIL 300
#define S_EVAL 301
#define S_SEVAL 302
#define S_ERROR 303
#define S_FILL 304
#define S_SHIFT 305
#define S_GETNUM 306
#define S_GETSTRING 307
#define S_GETEXP 308
#define S_GETFMT 309
#define S_GETFORMAT 310
#define S_RECALC 311
#define S_QUIT 312
#define S_REBUILD_GRAPH 313
#define S_PRINT_GRAPH 314
#define S_SYNCREFS 315
#define S_REDO 316
#define S_UNDO 317
#define S_IMAP 318
#define S_NMAP 319
#define S_INOREMAP 320
#define S_NNOREMAP 321
#define S_NUNMAP 322
#define S_IUNMAP 323
#define S_COLOR 324
#define S_CELLCOLOR 325
#define S_UNFORMAT 326
#define S_REDEFINE_COLOR 327
#define S_SET 328
#define S_FCOPY 329
#define S_FSUM 330
#define S_TRIGGER 331
#define S_UNTRIGGER 332
#define K_AUTOBACKUP 333
#define K_NOAUTOBACKUP 334
#define K_AUTOCALC 335
#define K_NOAUTOCALC 336
#define K_DEBUG 337
#define K_NODEBUG 338
#define K_TRG 339
#define K_NOTRG 340
#define K_EXTERNAL_FUNCTIONS 341
#define K_NOEXTERNAL_FUNCTIONS 342
#define K_HALF_PAGE_SCROLL 343
#define K_NOHALF_PAGE_SCROLL 344
#define K_NOCURSES 345
#define K_CURSES 346
#define K_NUMERIC 347
#define K_NONUMERIC 348
#define K_NUMERIC_DECIMAL 349
#define K_NONUMERIC_DECIMAL 350
#define K_NUMERIC_ZERO 351
#define K_NONUMERIC_ZERO 352
#define K_OVERLAP 353
#define K_NOOVERLAP 354
#define K_QUIT_AFTERLOAD 355
#define K_NOQUIT_AFTERLOAD 356
#define K_XLSX_READFORMULAS 357
#define K_NOXLSX_READFORMULAS 358
#define K_DEFAULT_COPY_TO_CLIPBOARD_CMD 359
#define K_DEFAULT_PASTE_FROM_CLIPBOARD_CMD 360
#define K_COPY_TO_CLIPBOARD_DELIMITED_TAB 361
#define K_NOCOPY_TO_CLIPBOARD_DELIMITED_TAB 362
#define K_IGNORECASE 363
#define K_NOIGNORECASE 364
#define K_TM_GMTOFF 365
#define K_NEWLINE_ACTION 366
#define K_ERROR 367
#define K_INVALID 368
#define K_FIXED 369
#define K_SUM 370
#define K_PROD 371
#define K_AVG 372
#define K_STDDEV 373
#define K_COUNT 374
#define K_ROWS 375
#define K_COLS 376
#define K_ABS 377
#define K_FROW 378
#define K_FCOL 379
#define K_ACOS 380
#define K_ASIN 381
#define K_ATAN 382
#define K_ATAN2 383
#define K_CEIL 384
#define K_COS 385
#define K_EXP 386
#define K_FABS 387
#define K_FLOOR 388
#define K_HYPOT 389
#define K_LN 390
#define K_LOG 391
#define K_PI 392
#define K_POW 393
#define K_SIN 394
#define K_SQRT 395
#define K_TAN 396
#define K_DTR 397
#define K_RTD 398
#define K_MAX 399
#define K_MIN 400
#define K_RND 401
#define K_ROUND 402
#define K_IF 403
#define K_PV 404
#define K_FV 405
#define K_PMT 406
#define K_HOUR 407
#define K_MINUTE 408
#define K_SECOND 409
#define K_MONTH 410
#define K_DAY 411
#define K_YEAR 412
#define K_NOW 413
#define K_DATE 414
#define K_DTS 415
#define K_TTS 416
#define K_FMT 417
#define K_REPLACE 418
#define K_SUBSTR 419
#define K_UPPER 420
#define K_LOWER 421
#define K_CAPITAL 422
#define K_STON 423
#define K_SLEN 424
#define K_EQS 425
#define K_EXT 426
#define K_LUA 427
#define K_NVAL 428
#define K_SVAL 429
#define K_LOOKUP 430
#define K_HLOOKUP 431
#define K_VLOOKUP 432
#define K_INDEX 433
#define K_STINDEX 434
#define K_TBLSTYLE 435
#define K_TBL 436
#define K_LATEX 437
#define K_SLATEX 438
#define K_TEX 439
#define K_FRAME 440
#define K_RNDTOEVEN 441
#define K_FILENAME 442
#define K_MYROW 443
#define K_MYCOL 444
#define K_LASTROW 445
#define K_LASTCOL 446
#define K_COLTOA 447
#define K_CRACTION 448
#define K_CRROW 449
#define K_CRCOL 450
#define K_ROWLIMIT 451
#define K_COLLIMIT 452
#define K_PAGESIZE 453
#define K_ERR 454
#define K_REF 455
#define K_LOCALE 456
#define K_SET8BIT 457
#define K_ASCII 458
#define K_CHR 459

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED

union YYSTYPE
{
#line 43 "gram.y" /* yacc.c:355  */

    int ival;
    double fval;
    struct ent_ptr ent;
    struct enode * enode;
    char * sval;
    struct range_s rval;

#line 565 "y.tab.c" /* yacc.c:355  */
};

typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);

#endif /* !YY_YY_Y_TAB_H_INCLUDED  */

/* Copy the second part of user declarations.  */

#line 582 "y.tab.c" /* yacc.c:358  */

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif

#ifndef YY_ATTRIBUTE
# if (defined __GNUC__                                               \
      && (2 < __GNUC__ || (__GNUC__ == 2 && 96 <= __GNUC_MINOR__)))  \
     || defined __SUNPRO_C && 0x5110 <= __SUNPRO_C
#  define YY_ATTRIBUTE(Spec) __attribute__(Spec)
# else
#  define YY_ATTRIBUTE(Spec) /* empty */
# endif
#endif

#ifndef YY_ATTRIBUTE_PURE
# define YY_ATTRIBUTE_PURE   YY_ATTRIBUTE ((__pure__))
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# define YY_ATTRIBUTE_UNUSED YY_ATTRIBUTE ((__unused__))
#endif

#if !defined _Noreturn \
     && (!defined __STDC_VERSION__ || __STDC_VERSION__ < 201112)
# if defined _MSC_VER && 1200 <= _MSC_VER
#  define _Noreturn __declspec (noreturn)
# else
#  define _Noreturn YY_ATTRIBUTE ((__noreturn__))
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif

#if defined __GNUC__ && 407 <= __GNUC__ * 100 + __GNUC_MINOR__
/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN \
    _Pragma ("GCC diagnostic push") \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")\
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# define YY_IGNORE_MAYBE_UNINITIALIZED_END \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif


#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYSIZE_T yynewbytes;                                            \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / sizeof (*yyptr);                          \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, (Count) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYSIZE_T yyi;                         \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  161
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   2879

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  228
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  12
/* YYNRULES -- Number of rules.  */
#define YYNRULES  290
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  781

/* YYTRANSLATE[YYX] -- Symbol number corresponding to YYX as returned
   by yylex, with out-of-bounds checking.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   459

#define YYTRANSLATE(YYX)                                                \
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, without out-of-bounds checking.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,   213,     2,   216,   227,   219,   209,     2,
     222,   223,   217,   214,   224,   215,   225,   218,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,   207,   205,
     210,   211,   212,   206,   221,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,   220,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,   208,     2,   226,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    91,    92,    93,    94,
      95,    96,    97,    98,    99,   100,   101,   102,   103,   104,
     105,   106,   107,   108,   109,   110,   111,   112,   113,   114,
     115,   116,   117,   118,   119,   120,   121,   122,   123,   124,
     125,   126,   127,   128,   129,   130,   131,   132,   133,   134,
     135,   136,   137,   138,   139,   140,   141,   142,   143,   144,
     145,   146,   147,   148,   149,   150,   151,   152,   153,   154,
     155,   156,   157,   158,   159,   160,   161,   162,   163,   164,
     165,   166,   167,   168,   169,   170,   171,   172,   173,   174,
     175,   176,   177,   178,   179,   180,   181,   182,   183,   184,
     185,   186,   187,   188,   189,   190,   191,   192,   193,   194,
     195,   196,   197,   198,   199,   200,   201,   202,   203,   204
};

#if YYDEBUG
  /* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,   383,   383,   385,   404,   447,   448,   449,   450,   451,
     452,   453,   454,   458,   460,   463,   464,   465,   466,   469,
     471,   473,   475,   477,   479,   481,   493,   506,   517,   519,
     525,   527,   529,   530,   531,   532,   533,   534,   536,   540,
     545,   550,   561,   562,   564,   565,   566,   567,   569,   571,
     575,   576,   577,   578,   579,   585,   590,   596,   602,   607,
     612,   619,   628,   633,   637,   645,   651,   657,   661,   662,
     663,   664,   665,   667,   675,   688,   689,   691,   693,   697,
     701,   706,   707,   708,   715,   724,   729,   733,   735,   737,
     739,   741,   744,   748,   749,   758,   759,   760,   762,   764,
     766,   768,   770,   772,   774,   776,   778,   780,   782,   784,
     786,   788,   790,   792,   794,   796,   798,   799,   800,   801,
     802,   803,   804,   806,   807,   808,   809,   810,   811,   813,
     814,   815,   817,   818,   819,   820,   821,   822,   823,   825,
     827,   829,   831,   833,   834,   835,   836,   837,   838,   839,
     840,   842,   846,   848,   849,   850,   852,   853,   855,   857,
     858,   859,   860,   862,   864,   866,   868,   870,   872,   874,
     876,   878,   880,   882,   884,   886,   892,   894,   896,   898,
     900,   901,   903,   904,   905,   906,   907,   908,   909,   911,
     912,   913,   914,   915,   916,   917,   918,   919,   920,   921,
     922,   937,   938,   939,   940,   941,   942,   943,   944,   945,
     946,   947,   948,   949,   950,   951,   952,   953,   954,   955,
     956,   959,   960,   963,   964,   967,   970,   973,   976,   980,
     985,   986,   989,   990,   991,   992,   995,   996,  1008,  1009,
    1014,  1016,  1017,  1018,  1022,  1023,  1024,  1026,  1027,  1028,
    1030,  1031,  1032,  1034,  1035,  1036,  1039,  1040,  1041,  1044,
    1045,  1046,  1049,  1050,  1051,  1054,  1055,  1056,  1058,  1059,
    1060,  1062,  1063,  1064,  1066,  1067,  1068,  1071,  1072,  1073,
    1076,  1077,  1078,  1084,  1091,  1099,  1101,  1104,  1106,  1108,
    1109
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || 0
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "STRING", "COL", "NUMBER", "FNUMBER",
  "RANGE", "VAR", "WORD", "MAPWORD", "PLUGIN", "S_SHOW", "S_HIDE",
  "S_SHOWROW", "S_HIDEROW", "S_SHOWCOL", "S_HIDECOL", "S_FREEZE",
  "S_UNFREEZE", "S_MARK", "S_AUTOJUS", "S_PAD", "S_DATEFMT", "S_SUBTOTAL",
  "S_RSUBTOTAL", "S_FORMAT", "S_FMT", "S_LET", "S_LABEL", "S_LEFTSTRING",
  "S_RIGHTSTRING", "S_LEFTJUSTIFY", "S_RIGHTJUSTIFY", "S_CENTER", "S_SORT",
  "S_FILTERON", "S_GOTO", "S_CCOPY", "S_CPASTE", "S_PLOT", "S_LOCK",
  "S_UNLOCK", "S_DEFINE", "S_UNDEFINE", "S_DETAIL", "S_EVAL", "S_SEVAL",
  "S_ERROR", "S_FILL", "S_SHIFT", "S_GETNUM", "S_GETSTRING", "S_GETEXP",
  "S_GETFMT", "S_GETFORMAT", "S_RECALC", "S_QUIT", "S_REBUILD_GRAPH",
  "S_PRINT_GRAPH", "S_SYNCREFS", "S_REDO", "S_UNDO", "S_IMAP", "S_NMAP",
  "S_INOREMAP", "S_NNOREMAP", "S_NUNMAP", "S_IUNMAP", "S_COLOR",
  "S_CELLCOLOR", "S_UNFORMAT", "S_REDEFINE_COLOR", "S_SET", "S_FCOPY",
  "S_FSUM", "S_TRIGGER", "S_UNTRIGGER", "K_AUTOBACKUP", "K_NOAUTOBACKUP",
  "K_AUTOCALC", "K_NOAUTOCALC", "K_DEBUG", "K_NODEBUG", "K_TRG", "K_NOTRG",
  "K_EXTERNAL_FUNCTIONS", "K_NOEXTERNAL_FUNCTIONS", "K_HALF_PAGE_SCROLL",
  "K_NOHALF_PAGE_SCROLL", "K_NOCURSES", "K_CURSES", "K_NUMERIC",
  "K_NONUMERIC", "K_NUMERIC_DECIMAL", "K_NONUMERIC_DECIMAL",
  "K_NUMERIC_ZERO", "K_NONUMERIC_ZERO", "K_OVERLAP", "K_NOOVERLAP",
  "K_QUIT_AFTERLOAD", "K_NOQUIT_AFTERLOAD", "K_XLSX_READFORMULAS",
  "K_NOXLSX_READFORMULAS", "K_DEFAULT_COPY_TO_CLIPBOARD_CMD",
  "K_DEFAULT_PASTE_FROM_CLIPBOARD_CMD",
  "K_COPY_TO_CLIPBOARD_DELIMITED_TAB",
  "K_NOCOPY_TO_CLIPBOARD_DELIMITED_TAB", "K_IGNORECASE", "K_NOIGNORECASE",
  "K_TM_GMTOFF", "K_NEWLINE_ACTION", "K_ERROR", "K_INVALID", "K_FIXED",
  "K_SUM", "K_PROD", "K_AVG", "K_STDDEV", "K_COUNT", "K_ROWS", "K_COLS",
  "K_ABS", "K_FROW", "K_FCOL", "K_ACOS", "K_ASIN", "K_ATAN", "K_ATAN2",
  "K_CEIL", "K_COS", "K_EXP", "K_FABS", "K_FLOOR", "K_HYPOT", "K_LN",
  "K_LOG", "K_PI", "K_POW", "K_SIN", "K_SQRT", "K_TAN", "K_DTR", "K_RTD",
  "K_MAX", "K_MIN", "K_RND", "K_ROUND", "K_IF", "K_PV", "K_FV", "K_PMT",
  "K_HOUR", "K_MINUTE", "K_SECOND", "K_MONTH", "K_DAY", "K_YEAR", "K_NOW",
  "K_DATE", "K_DTS", "K_TTS", "K_FMT", "K_REPLACE", "K_SUBSTR", "K_UPPER",
  "K_LOWER", "K_CAPITAL", "K_STON", "K_SLEN", "K_EQS", "K_EXT", "K_LUA",
  "K_NVAL", "K_SVAL", "K_LOOKUP", "K_HLOOKUP", "K_VLOOKUP", "K_INDEX",
  "K_STINDEX", "K_TBLSTYLE", "K_TBL", "K_LATEX", "K_SLATEX", "K_TEX",
  "K_FRAME", "K_RNDTOEVEN", "K_FILENAME", "K_MYROW", "K_MYCOL",
  "K_LASTROW", "K_LASTCOL", "K_COLTOA", "K_CRACTION", "K_CRROW", "K_CRCOL",
  "K_ROWLIMIT", "K_COLLIMIT", "K_PAGESIZE", "K_ERR", "K_REF", "K_LOCALE",
  "K_SET8BIT", "K_ASCII", "K_CHR", "';'", "'?'", "':'", "'|'", "'&'",
  "'<'", "'='", "'>'", "'!'", "'+'", "'-'", "'#'", "'*'", "'/'", "'%'",
  "'^'", "'@'", "'('", "')'", "','", "'.'", "'~'", "'$'", "$accept",
  "command", "term", "e", "expr_list", "range", "var", "var_or_range",
  "num", "strarg", "setlist", "setitem", YY_NULLPTR
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[NUM] -- (External) token number corresponding to the
   (internal) symbol number NUM (which must be that of a token).  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301,   302,   303,   304,
     305,   306,   307,   308,   309,   310,   311,   312,   313,   314,
     315,   316,   317,   318,   319,   320,   321,   322,   323,   324,
     325,   326,   327,   328,   329,   330,   331,   332,   333,   334,
     335,   336,   337,   338,   339,   340,   341,   342,   343,   344,
     345,   346,   347,   348,   349,   350,   351,   352,   353,   354,
     355,   356,   357,   358,   359,   360,   361,   362,   363,   364,
     365,   366,   367,   368,   369,   370,   371,   372,   373,   374,
     375,   376,   377,   378,   379,   380,   381,   382,   383,   384,
     385,   386,   387,   388,   389,   390,   391,   392,   393,   394,
     395,   396,   397,   398,   399,   400,   401,   402,   403,   404,
     405,   406,   407,   408,   409,   410,   411,   412,   413,   414,
     415,   416,   417,   418,   419,   420,   421,   422,   423,   424,
     425,   426,   427,   428,   429,   430,   431,   432,   433,   434,
     435,   436,   437,   438,   439,   440,   441,   442,   443,   444,
     445,   446,   447,   448,   449,   450,   451,   452,   453,   454,
     455,   456,   457,   458,   459,    59,    63,    58,   124,    38,
      60,    61,    62,    33,    43,    45,    35,    42,    47,    37,
      94,    64,    40,    41,    44,    46,   126,    36
};
# endif

#define YYPACT_NINF -374

#define yypact_value_is_default(Yystate) \
  (!!((Yystate) == (-374)))

#define YYTABLE_NINF -232

#define yytable_value_is_error(Yytable_value) \
  (!!((Yytable_value) == (-232)))

  /* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
     STATE-NUM.  */
static const yytype_int16 yypact[] =
{
     993,  -374,    18,    46,    14,    19,     1,    27,    37,  -374,
      53,    65,    76,    13,    39,    39,    79,    39,    39,    39,
      39,    39,    39,    39,    39,    39,    39,    22,    39,  -374,
      89,    39,    39,    32,    39,     0,   274,   274,    90,    48,
      39,    39,    39,    39,    39,    82,  -374,  -374,  -374,  -374,
    -374,  -374,  -374,    92,   100,   101,   105,   106,   110,   123,
      30,    39,   124,  -374,  -374,  -374,    39,    39,   133,  -374,
    -374,  -374,  -374,   -73,   -72,   -65,   -64,     4,   -63,  -374,
    -374,   145,  -374,   -57,    39,   -56,    54,  -374,    34,  -374,
     -57,   149,   150,   151,   148,   153,   -52,   -51,   -50,   -49,
    -374,  -374,  -374,   175,  -374,  -374,  -374,  -374,    83,    83,
     177,   179,    39,  -374,  -374,    39,  -374,  -374,  -374,  -374,
      39,  -374,  -374,  -374,   -39,  -374,  -374,  -374,   562,   562,
     274,   676,   321,   562,  -374,  2659,  -374,  2659,  -374,    83,
      83,   197,  -374,  -374,  -374,  -374,  -374,   198,   201,   202,
     205,  -374,  -374,  -374,  -374,   207,  -374,   208,   994,   211,
    -374,  -374,   210,   218,   212,   220,  -374,   221,   227,   230,
      44,     0,    39,   222,    63,  -374,  -374,   236,   255,   260,
    -374,   274,   274,   274,   274,  -374,  -374,  -374,  -374,  -374,
    -374,  -374,  -374,   -57,   262,  -374,  -374,  -154,   562,     8,
      50,    61,    62,    64,    69,    70,    77,    81,    84,    88,
      91,    95,    96,   108,   170,   171,   172,   173,   174,   178,
     181,  -374,   195,   203,   204,   206,   209,   224,   228,   229,
     231,   232,   233,   234,   242,   243,   244,   245,   246,   247,
     248,   249,  -374,   250,   253,   254,   261,   268,   269,   270,
     271,   272,   275,   276,   277,   291,   300,   301,   303,   304,
     309,   311,   315,   316,   317,  -374,  -374,  -374,  -374,   318,
    -374,  -374,   319,   322,   323,   767,  1536,  -374,   274,   274,
     274,   274,     7,   274,    74,    93,   274,   274,   274,   274,
     274,   274,   274,    83,  -374,  -374,  -374,  -374,  -374,  -374,
    -374,   306,   103,  -374,   111,  -374,   112,  -374,   117,  -374,
     186,  -374,   190,  -374,   213,  -374,   216,  -374,   219,  -374,
     238,  -374,   241,  -374,   273,  -374,   335,  -374,   338,   339,
     341,  -374,   343,  -374,   345,   347,  -374,  -374,  -374,  -374,
    -374,  -374,  -374,  -374,  -374,  -374,   307,  -374,  -374,  -374,
     414,   425,   428,   314,  2659,  2659,  2659,  2659,    51,  -374,
      39,    39,    39,    39,    39,    39,    39,   274,   274,   274,
     274,   274,   274,   274,   274,   274,   274,   274,   274,   274,
     274,   274,   274,   274,   274,   274,   274,   274,    94,    94,
     274,   274,   274,   274,   274,   274,   274,   274,   274,   274,
     274,   274,   274,   274,   274,   274,   274,   274,   274,   274,
     274,   274,   274,   274,   274,   274,   274,   274,    94,    94,
      94,    94,    94,   274,   274,   274,   274,   274,   524,  -374,
    2659,  2643,   196,   225,   274,   274,   292,   292,   274,   292,
     274,  -154,  -154,  -154,    85,    85,    85,  -374,  -374,   477,
     509,   557,   558,   559,   566,   567,   568,   569,   571,   572,
     574,   575,   576,    32,    32,   577,   578,    83,     9,  -374,
    -374,  -374,  -374,  -374,   579,   362,   364,   365,   367,   372,
     374,   375,   377,   378,   386,   387,   388,  1552,  1568,  1584,
    1600,  1616,  1632,   941,  1648,  1664,  1680,  1696,  1712,   958,
    1728,  1744,   975,  1760,  1776,  1792,  1808,  1824,   992,   389,
    -175,   396,  1009,   397,   400,  1840,  1026,  1043,  1060,  1077,
    1094,  1856,  1872,  1888,  1904,  1920,  1936,   -21,  1111,  1128,
    1145,  1162,  1179,  1952,  1968,  1984,  2000,  2016,  1196,  1213,
    1230,  1247,  1264,  1281,   401,   -57,  1298,   403,  1315,   405,
    1332,   406,  1349,   407,  2032,  2048,  2064,  2080,  2096,   562,
     274,   292,   292,   292,   292,  -374,  -374,  -374,  -374,  -374,
    -374,  -374,  -374,  -374,  -374,  -374,  -374,  -374,  -374,  -374,
    -374,  -374,  -374,  -374,  -374,  -374,  -374,   274,  -374,   274,
    -374,   274,  -374,   274,  -374,   274,  -374,  -374,  -374,  -374,
    -374,  -374,  -374,  -374,  -374,   274,  -374,  -374,  -374,  -374,
    -374,   274,  -374,  -374,   274,  -374,  -374,  -374,  -374,  -374,
     274,   274,  -374,   274,   274,  -374,  -374,   274,   274,   274,
     274,   274,  -374,  -374,  -374,  -374,  -374,  -374,  -374,   274,
     274,   274,   274,   274,   274,  -374,  -374,  -374,  -374,  -374,
     274,   274,   274,   274,   274,    39,   274,    39,   274,    39,
     274,    39,   274,    39,   274,  -374,  -374,  -374,  -374,  -374,
    -374,   -43,  2112,  2128,  2144,  2160,  2176,  2192,  2208,  2224,
    2659,  -164,  2240,  -153,  2256,  2272,  1366,  1383,  1400,  1417,
    2288,  1434,  1451,  2304,  1468,  1485,  2320,  2336,  2352,  2368,
    2384,   417,  2400,   419,  1502,   420,  1519,   418,   901,   422,
     921,  -374,  -374,  -374,  -374,  -374,  -374,  -374,  -374,  -374,
     274,  -374,  -374,  -374,  -374,   274,   274,   274,   274,  -374,
     274,   274,  -374,   274,   274,  -374,  -374,  -374,  -374,  -374,
    -374,  -374,   274,   274,   274,   274,  -374,  -374,   274,  -374,
    -374,   274,  2659,  2416,  2432,  2448,  2464,  2480,  2496,  2512,
    2528,  2544,  2560,  2576,  2592,  2608,  2624,  -374,  -374,  -374,
    -374,  -374,  -374,  -374,  -374,  -374,  -374,  -374,  -374,  -374,
    -374
};

  /* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
     Performed when YYTABLE does not specify something else to do.  Zero
     means the default is an error.  */
static const yytype_uint16 yydefact[] =
{
       0,    94,     0,     0,     0,     0,     0,     0,     0,    32,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,    51,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,    85,    79,    80,    81,
      82,    84,    83,     0,     0,     0,     0,     0,     0,     0,
       0,    66,     0,   238,    68,    69,     0,     0,     0,    17,
      18,    15,    16,    22,    21,    20,    19,    37,    35,   224,
     229,     0,    33,     0,     0,    43,     0,    14,     0,   230,
     231,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       8,     9,    10,     0,    41,    47,   232,   233,     0,     0,
       0,     0,    45,    46,    50,     0,    52,    53,   236,   237,
       0,    77,     3,   185,   182,   183,   198,   200,     0,     0,
       0,     0,     0,     0,   208,    78,    95,    91,    92,     0,
       0,     0,    86,    87,    88,    90,    89,     0,     0,     0,
       0,    58,    59,    60,    64,     0,    65,     0,    74,     0,
      63,     1,     0,     0,     0,     0,   225,     0,     0,     0,
       0,     0,    28,     0,    71,    72,    13,     0,     0,     0,
      12,     4,     0,     0,     0,    38,   235,   234,    48,    49,
      44,    73,    75,    76,     0,   187,   181,   206,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   184,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   149,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   189,   190,   191,   192,     0,
     197,   199,     0,     0,     0,     0,     0,   186,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    31,    27,    55,    54,    57,    56,
      61,     0,     0,   244,   245,   247,   248,   250,   251,   253,
     254,   256,   257,   259,   266,   268,   269,   271,   275,   277,
     278,   280,   241,   242,   260,   262,   263,   265,     0,     0,
     285,   287,   272,   274,   289,   281,   239,    62,    24,    26,
      23,    25,    36,   227,    34,   226,     0,   223,    29,    42,
       0,     0,     0,     0,     2,     5,     6,     7,     0,    96,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   180,
     210,     0,   215,   214,     0,     0,   211,   212,     0,   213,
       0,   201,   202,   220,   203,   204,   205,   207,    30,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   228,
      70,    39,    40,    11,     0,   230,     0,   230,     0,   230,
       0,   230,     0,   230,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   230,
      95,     0,     0,   230,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,    95,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   216,   218,   219,   217,    67,   243,   246,   249,   252,
     255,   258,   267,   270,   276,   279,   240,   261,   264,   283,
     284,   286,   273,   290,   288,   282,   151,     0,    98,     0,
     100,     0,   102,     0,   104,     0,   106,   114,   115,   116,
     117,   118,   119,   120,   121,     0,   123,   124,   125,   126,
     127,     0,   129,   130,     0,   132,   133,   134,   135,   136,
       0,     0,   108,     0,     0,   111,   137,     0,     0,     0,
       0,     0,   143,   144,   145,   146,   147,   148,   156,     0,
       0,     0,     0,     0,     0,   159,   160,   161,   153,   154,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   188,   193,   195,   194,   196,
      97,   209,     0,     0,     0,     0,     0,     0,     0,     0,
     221,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    99,   101,   103,   105,   107,   122,   128,   131,   110,
       0,   109,   113,   112,   138,     0,     0,     0,     0,   157,
       0,     0,   158,     0,     0,   155,   174,   175,   176,   177,
     166,   165,     0,     0,     0,     0,   163,   162,     0,   172,
     171,     0,   222,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   139,   140,   141,
     142,   150,   152,   178,   179,   168,   167,   170,   169,   164,
     173
};

  /* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
    -374,  -374,  -126,   -36,   -25,    59,    97,   615,   -33,  -373,
    -374,  -374
};

  /* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,    68,   134,   680,   681,    89,   136,    91,   113,   120,
     158,   336
};

  /* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
     positive, shift that token.  If negative, reduce the rule whose
     number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_int16 yytable[] =
{
     135,   137,   195,   196,    88,    75,   140,   277,    80,   166,
     123,    88,   124,   125,   584,    80,    87,    88,   585,    73,
      79,    80,    69,    70,    74,   105,    88,   106,   107,    79,
      80,    76,   171,   154,    88,   118,    88,    79,    80,   166,
      80,    77,    78,    88,    79,    80,    79,    80,  -231,   345,
      71,    72,    88,   106,   107,    79,    80,    84,   174,   719,
     720,    79,    80,   289,   290,   291,   292,    82,   166,    85,
     722,   720,   359,    92,    93,   186,   187,   123,    88,   124,
     125,    86,    80,    94,   103,   104,   146,   114,   106,   107,
     579,   580,   115,   138,   197,   147,   276,   123,    88,   124,
     125,    79,    80,   148,   149,    83,   293,   294,   150,   151,
      90,    83,    83,   152,    90,    90,    90,    90,    90,    90,
      90,    90,    83,    83,    90,    83,   153,   157,    90,    90,
     119,    90,   122,   161,   162,   163,    90,    90,    90,    90,
      90,    90,   164,   165,   169,   354,   355,   356,   357,   170,
     171,   173,   176,   179,   177,   178,   180,    90,    90,   181,
     182,   183,   184,    90,    90,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   185,   192,
     188,    90,   189,    90,   278,   279,   194,   280,   281,   282,
     283,   284,   285,   286,   287,   288,   289,   290,   291,   292,
     295,   296,   638,   639,   297,   298,   126,   127,   299,    90,
     300,   167,    90,   301,   337,   338,   340,   193,   434,   435,
     128,   129,   130,   339,   341,   342,   349,    81,   131,   132,
     360,   168,   343,   133,    81,   344,   108,   109,   110,   351,
      81,   111,   430,   431,   432,   433,   436,   437,   439,    81,
     441,   442,   443,   444,   445,   446,   447,    81,   352,    81,
     448,   168,   108,   109,    81,   353,    81,   358,   347,    90,
     350,   346,   361,   126,   127,    81,   474,   123,    88,   124,
     125,    81,    80,   362,   363,   438,   364,   128,   129,   130,
     168,   365,   366,   126,   127,   131,   132,   108,   109,   367,
     133,    81,   359,   368,   440,   292,   369,   128,   129,   130,
     370,   449,   469,   371,   450,   131,   132,   372,   373,   473,
     133,    81,   451,   452,   123,    88,   124,   125,   453,    80,
     374,   487,   488,   489,   490,   491,   492,   493,   494,   495,
     496,   497,   498,   499,   500,   501,   502,   503,   504,   505,
     506,   507,   508,   512,   515,   516,   517,   518,   519,   520,
     521,   522,   523,   524,   525,   526,   527,   528,   529,   530,
     531,   532,   533,   534,   535,   536,   537,   538,   539,   540,
     541,   542,   543,   546,   548,   550,   552,   554,   555,   556,
     557,   558,   375,   376,   377,   378,   379,   454,   561,   562,
     380,   455,   563,   381,   564,   281,   282,   283,   284,   285,
     286,   287,   288,   289,   290,   291,   292,   382,   470,   475,
     477,   479,   481,   483,   456,   383,   384,   457,   385,   471,
     458,   386,   472,   670,   583,   282,   283,   284,   285,   286,
     287,   288,   289,   290,   291,   292,   387,   509,   513,   459,
     388,   389,   460,   390,   391,   392,   393,    90,    90,    90,
      90,    90,    90,    90,   394,   395,   396,   397,   398,   399,
     400,   401,   402,   126,   127,   403,   404,   544,   547,   549,
     551,   553,   565,   405,   461,   510,   510,   128,   129,   130,
     406,   407,   408,   409,   410,   131,   132,   411,   412,   413,
     133,    81,  -232,  -232,  -232,  -232,   286,   287,   288,   289,
     290,   291,   292,   414,   566,   545,   545,   545,   545,   545,
     126,   127,   415,   416,   671,   417,   418,   123,    88,   124,
     125,   419,    80,   420,   128,   129,   130,   421,   422,   423,
     424,   425,   275,   132,   426,   427,   462,   133,    81,   463,
     464,   672,   465,   673,   466,   674,   467,   675,   468,   676,
     119,   119,   567,   568,   569,   123,    88,   124,   125,   677,
      80,   570,   571,   572,   573,   678,   574,   575,   679,   576,
     577,   578,   581,   582,   586,   682,   587,   588,   684,   589,
     590,   685,   686,   687,   688,   689,   591,   592,   683,   593,
     594,     0,   595,   690,   691,   692,   693,   694,   695,   596,
     597,   598,     0,   621,   696,   697,   698,   699,   700,   622,
     702,   624,   704,   625,   706,   656,   708,   658,   710,   660,
     662,   664,    95,    96,    97,    98,    99,   100,   101,   102,
     740,   746,   112,   742,   744,   749,   116,   117,     0,   121,
       0,     0,     0,     0,   139,   141,   142,   143,   144,   145,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   155,   156,     0,     0,     0,
       0,   159,   160,     0,   752,     0,     0,     0,     0,   753,
     754,   755,   756,     0,   757,   758,     0,   759,   760,   172,
       0,   175,     0,     0,     0,     0,   761,   762,   763,   764,
       0,     0,   765,     0,   701,   766,   703,     0,   705,     0,
     707,     0,   709,   126,   127,     0,     0,   190,     0,     0,
     191,     0,     0,     0,     0,     0,     0,   128,   129,     0,
       0,     0,     0,     0,     0,   131,   132,   559,     0,     0,
     133,    81,    83,     0,    83,     0,    83,     0,    83,     0,
      83,   126,   127,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   128,   129,     0,     0,     0,
       0,     0,     0,   131,   132,     0,     0,   348,   133,    81,
     198,   199,   200,   201,   202,   203,   204,   205,   206,   207,
     208,   209,   210,   211,   212,   213,   214,   215,   216,   217,
     218,   219,   220,   221,   222,   223,   224,   225,   226,   227,
     228,   229,   230,   231,   232,   233,   234,   235,   236,   237,
     238,   239,   240,   241,   242,   243,   244,   245,   246,   247,
     248,   249,   250,   251,   252,   253,   254,   255,   256,   257,
     258,   259,   260,   261,   262,   263,     0,     0,     0,     0,
       0,     0,     0,   264,   265,   266,   267,   268,   269,     0,
       0,     0,     0,     0,     0,   270,   271,     0,   272,   273,
     274,   428,   199,   200,   201,   202,   203,   204,   205,   206,
     207,   208,   209,   210,   211,   212,   213,   214,   215,   216,
     217,   218,   219,   220,   221,   222,   223,   224,   225,   226,
     227,   228,   229,   230,   231,   232,   233,   234,   235,   236,
     237,   238,   239,   240,   241,   242,   243,   244,   245,   246,
     247,   248,   249,   250,   251,   252,   253,   254,   255,   256,
     257,   258,   259,   260,   261,   262,   263,     0,     0,     0,
       0,     0,     0,     0,   264,   265,   266,   267,   268,   269,
       0,     0,     0,     0,     0,     0,   270,   271,     0,   272,
     273,   274,     0,     0,     0,   476,   478,   480,   482,   484,
     485,   486,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   -93,     1,     0,     0,     0,     0,     0,
       0,     0,     0,   511,   514,     2,     3,     4,     5,     6,
       7,     8,     9,    10,    11,    12,    13,    14,    15,    16,
      17,    18,    19,    20,    21,    22,    23,    24,    25,    26,
      27,    28,    29,    30,    31,    32,    33,    34,    35,    36,
      37,    38,    39,    40,    41,    42,    43,    44,    45,    46,
      47,    48,    49,    50,    51,    52,    53,    54,    55,    56,
      57,    58,    59,    60,    61,    62,    63,    64,    65,    66,
      67,     0,   302,   303,   304,   305,   306,   307,   308,   309,
     310,   311,   312,   313,   314,   315,   316,   317,   318,   319,
     320,   321,   322,   323,   324,   325,   326,   327,   328,   329,
     330,   331,   332,   333,   334,   335,   278,   279,     0,   280,
     281,   282,   283,   284,   285,   286,   287,   288,   289,   290,
     291,   292,     0,     0,   747,   748,   278,   279,     0,   280,
     281,   282,   283,   284,   285,   286,   287,   288,   289,   290,
     291,   292,     0,     0,   750,   751,   278,   279,     0,   280,
     281,   282,   283,   284,   285,   286,   287,   288,   289,   290,
     291,   292,     0,   278,   279,   605,   280,   281,   282,   283,
     284,   285,   286,   287,   288,   289,   290,   291,   292,     0,
     278,   279,   611,   280,   281,   282,   283,   284,   285,   286,
     287,   288,   289,   290,   291,   292,     0,   278,   279,   614,
     280,   281,   282,   283,   284,   285,   286,   287,   288,   289,
     290,   291,   292,     0,   278,   279,   620,   280,   281,   282,
     283,   284,   285,   286,   287,   288,   289,   290,   291,   292,
       0,   278,   279,   623,   280,   281,   282,   283,   284,   285,
     286,   287,   288,   289,   290,   291,   292,     0,   278,   279,
     627,   280,   281,   282,   283,   284,   285,   286,   287,   288,
     289,   290,   291,   292,     0,   278,   279,   628,   280,   281,
     282,   283,   284,   285,   286,   287,   288,   289,   290,   291,
     292,     0,   278,   279,   629,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,     0,   278,
     279,   630,   280,   281,   282,   283,   284,   285,   286,   287,
     288,   289,   290,   291,   292,     0,   278,   279,   631,   280,
     281,   282,   283,   284,   285,   286,   287,   288,   289,   290,
     291,   292,     0,   278,   279,   640,   280,   281,   282,   283,
     284,   285,   286,   287,   288,   289,   290,   291,   292,     0,
     278,   279,   641,   280,   281,   282,   283,   284,   285,   286,
     287,   288,   289,   290,   291,   292,     0,   278,   279,   642,
     280,   281,   282,   283,   284,   285,   286,   287,   288,   289,
     290,   291,   292,     0,   278,   279,   643,   280,   281,   282,
     283,   284,   285,   286,   287,   288,   289,   290,   291,   292,
       0,   278,   279,   644,   280,   281,   282,   283,   284,   285,
     286,   287,   288,   289,   290,   291,   292,     0,   278,   279,
     650,   280,   281,   282,   283,   284,   285,   286,   287,   288,
     289,   290,   291,   292,     0,   278,   279,   651,   280,   281,
     282,   283,   284,   285,   286,   287,   288,   289,   290,   291,
     292,     0,   278,   279,   652,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,     0,   278,
     279,   653,   280,   281,   282,   283,   284,   285,   286,   287,
     288,   289,   290,   291,   292,     0,   278,   279,   654,   280,
     281,   282,   283,   284,   285,   286,   287,   288,   289,   290,
     291,   292,     0,   278,   279,   655,   280,   281,   282,   283,
     284,   285,   286,   287,   288,   289,   290,   291,   292,     0,
     278,   279,   657,   280,   281,   282,   283,   284,   285,   286,
     287,   288,   289,   290,   291,   292,     0,   278,   279,   659,
     280,   281,   282,   283,   284,   285,   286,   287,   288,   289,
     290,   291,   292,     0,   278,   279,   661,   280,   281,   282,
     283,   284,   285,   286,   287,   288,   289,   290,   291,   292,
       0,   278,   279,   663,   280,   281,   282,   283,   284,   285,
     286,   287,   288,   289,   290,   291,   292,     0,   278,   279,
     725,   280,   281,   282,   283,   284,   285,   286,   287,   288,
     289,   290,   291,   292,     0,   278,   279,   726,   280,   281,
     282,   283,   284,   285,   286,   287,   288,   289,   290,   291,
     292,     0,   278,   279,   727,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,     0,   278,
     279,   728,   280,   281,   282,   283,   284,   285,   286,   287,
     288,   289,   290,   291,   292,     0,   278,   279,   730,   280,
     281,   282,   283,   284,   285,   286,   287,   288,   289,   290,
     291,   292,     0,   278,   279,   731,   280,   281,   282,   283,
     284,   285,   286,   287,   288,   289,   290,   291,   292,     0,
     278,   279,   733,   280,   281,   282,   283,   284,   285,   286,
     287,   288,   289,   290,   291,   292,     0,   278,   279,   734,
     280,   281,   282,   283,   284,   285,   286,   287,   288,   289,
     290,   291,   292,     0,   278,   279,   743,   280,   281,   282,
     283,   284,   285,   286,   287,   288,   289,   290,   291,   292,
       0,   278,   279,   745,   280,   281,   282,   283,   284,   285,
     286,   287,   288,   289,   290,   291,   292,   278,   279,   429,
     280,   281,   282,   283,   284,   285,   286,   287,   288,   289,
     290,   291,   292,   278,   279,   599,   280,   281,   282,   283,
     284,   285,   286,   287,   288,   289,   290,   291,   292,   278,
     279,   600,   280,   281,   282,   283,   284,   285,   286,   287,
     288,   289,   290,   291,   292,   278,   279,   601,   280,   281,
     282,   283,   284,   285,   286,   287,   288,   289,   290,   291,
     292,   278,   279,   602,   280,   281,   282,   283,   284,   285,
     286,   287,   288,   289,   290,   291,   292,   278,   279,   603,
     280,   281,   282,   283,   284,   285,   286,   287,   288,   289,
     290,   291,   292,   278,   279,   604,   280,   281,   282,   283,
     284,   285,   286,   287,   288,   289,   290,   291,   292,   278,
     279,   606,   280,   281,   282,   283,   284,   285,   286,   287,
     288,   289,   290,   291,   292,   278,   279,   607,   280,   281,
     282,   283,   284,   285,   286,   287,   288,   289,   290,   291,
     292,   278,   279,   608,   280,   281,   282,   283,   284,   285,
     286,   287,   288,   289,   290,   291,   292,   278,   279,   609,
     280,   281,   282,   283,   284,   285,   286,   287,   288,   289,
     290,   291,   292,   278,   279,   610,   280,   281,   282,   283,
     284,   285,   286,   287,   288,   289,   290,   291,   292,   278,
     279,   612,   280,   281,   282,   283,   284,   285,   286,   287,
     288,   289,   290,   291,   292,   278,   279,   613,   280,   281,
     282,   283,   284,   285,   286,   287,   288,   289,   290,   291,
     292,   278,   279,   615,   280,   281,   282,   283,   284,   285,
     286,   287,   288,   289,   290,   291,   292,   278,   279,   616,
     280,   281,   282,   283,   284,   285,   286,   287,   288,   289,
     290,   291,   292,   278,   279,   617,   280,   281,   282,   283,
     284,   285,   286,   287,   288,   289,   290,   291,   292,   278,
     279,   618,   280,   281,   282,   283,   284,   285,   286,   287,
     288,   289,   290,   291,   292,   278,   279,   619,   280,   281,
     282,   283,   284,   285,   286,   287,   288,   289,   290,   291,
     292,   278,   279,   626,   280,   281,   282,   283,   284,   285,
     286,   287,   288,   289,   290,   291,   292,   278,   279,   632,
     280,   281,   282,   283,   284,   285,   286,   287,   288,   289,
     290,   291,   292,   278,   279,   633,   280,   281,   282,   283,
     284,   285,   286,   287,   288,   289,   290,   291,   292,   278,
     279,   634,   280,   281,   282,   283,   284,   285,   286,   287,
     288,   289,   290,   291,   292,   278,   279,   635,   280,   281,
     282,   283,   284,   285,   286,   287,   288,   289,   290,   291,
     292,   278,   279,   636,   280,   281,   282,   283,   284,   285,
     286,   287,   288,   289,   290,   291,   292,   278,   279,   637,
     280,   281,   282,   283,   284,   285,   286,   287,   288,   289,
     290,   291,   292,   278,   279,   645,   280,   281,   282,   283,
     284,   285,   286,   287,   288,   289,   290,   291,   292,   278,
     279,   646,   280,   281,   282,   283,   284,   285,   286,   287,
     288,   289,   290,   291,   292,   278,   279,   647,   280,   281,
     282,   283,   284,   285,   286,   287,   288,   289,   290,   291,
     292,   278,   279,   648,   280,   281,   282,   283,   284,   285,
     286,   287,   288,   289,   290,   291,   292,   278,   279,   649,
     280,   281,   282,   283,   284,   285,   286,   287,   288,   289,
     290,   291,   292,   278,   279,   665,   280,   281,   282,   283,
     284,   285,   286,   287,   288,   289,   290,   291,   292,   278,
     279,   666,   280,   281,   282,   283,   284,   285,   286,   287,
     288,   289,   290,   291,   292,   278,   279,   667,   280,   281,
     282,   283,   284,   285,   286,   287,   288,   289,   290,   291,
     292,   278,   279,   668,   280,   281,   282,   283,   284,   285,
     286,   287,   288,   289,   290,   291,   292,   278,   279,   669,
     280,   281,   282,   283,   284,   285,   286,   287,   288,   289,
     290,   291,   292,   278,   279,   711,   280,   281,   282,   283,
     284,   285,   286,   287,   288,   289,   290,   291,   292,   278,
     279,   712,   280,   281,   282,   283,   284,   285,   286,   287,
     288,   289,   290,   291,   292,   278,   279,   713,   280,   281,
     282,   283,   284,   285,   286,   287,   288,   289,   290,   291,
     292,   278,   279,   714,   280,   281,   282,   283,   284,   285,
     286,   287,   288,   289,   290,   291,   292,   278,   279,   715,
     280,   281,   282,   283,   284,   285,   286,   287,   288,   289,
     290,   291,   292,   278,   279,   716,   280,   281,   282,   283,
     284,   285,   286,   287,   288,   289,   290,   291,   292,   278,
     279,   717,   280,   281,   282,   283,   284,   285,   286,   287,
     288,   289,   290,   291,   292,   278,   279,   718,   280,   281,
     282,   283,   284,   285,   286,   287,   288,   289,   290,   291,
     292,   278,   279,   721,   280,   281,   282,   283,   284,   285,
     286,   287,   288,   289,   290,   291,   292,   278,   279,   723,
     280,   281,   282,   283,   284,   285,   286,   287,   288,   289,
     290,   291,   292,   278,   279,   724,   280,   281,   282,   283,
     284,   285,   286,   287,   288,   289,   290,   291,   292,   278,
     279,   729,   280,   281,   282,   283,   284,   285,   286,   287,
     288,   289,   290,   291,   292,   278,   279,   732,   280,   281,
     282,   283,   284,   285,   286,   287,   288,   289,   290,   291,
     292,   278,   279,   735,   280,   281,   282,   283,   284,   285,
     286,   287,   288,   289,   290,   291,   292,   278,   279,   736,
     280,   281,   282,   283,   284,   285,   286,   287,   288,   289,
     290,   291,   292,   278,   279,   737,   280,   281,   282,   283,
     284,   285,   286,   287,   288,   289,   290,   291,   292,   278,
     279,   738,   280,   281,   282,   283,   284,   285,   286,   287,
     288,   289,   290,   291,   292,   278,   279,   739,   280,   281,
     282,   283,   284,   285,   286,   287,   288,   289,   290,   291,
     292,   278,   279,   741,   280,   281,   282,   283,   284,   285,
     286,   287,   288,   289,   290,   291,   292,   278,   279,   767,
     280,   281,   282,   283,   284,   285,   286,   287,   288,   289,
     290,   291,   292,   278,   279,   768,   280,   281,   282,   283,
     284,   285,   286,   287,   288,   289,   290,   291,   292,   278,
     279,   769,   280,   281,   282,   283,   284,   285,   286,   287,
     288,   289,   290,   291,   292,   278,   279,   770,   280,   281,
     282,   283,   284,   285,   286,   287,   288,   289,   290,   291,
     292,   278,   279,   771,   280,   281,   282,   283,   284,   285,
     286,   287,   288,   289,   290,   291,   292,   278,   279,   772,
     280,   281,   282,   283,   284,   285,   286,   287,   288,   289,
     290,   291,   292,   278,   279,   773,   280,   281,   282,   283,
     284,   285,   286,   287,   288,   289,   290,   291,   292,   278,
     279,   774,   280,   281,   282,   283,   284,   285,   286,   287,
     288,   289,   290,   291,   292,   278,   279,   775,   280,   281,
     282,   283,   284,   285,   286,   287,   288,   289,   290,   291,
     292,   278,   279,   776,   280,   281,   282,   283,   284,   285,
     286,   287,   288,   289,   290,   291,   292,   278,   279,   777,
     280,   281,   282,   283,   284,   285,   286,   287,   288,   289,
     290,   291,   292,   278,   279,   778,   280,   281,   282,   283,
     284,   285,   286,   287,   288,   289,   290,   291,   292,   278,
     279,   779,   280,   281,   282,   283,   284,   285,   286,   287,
     288,   289,   290,   291,   292,     0,     0,   780,   278,   279,
     560,   280,   281,   282,   283,   284,   285,   286,   287,   288,
     289,   290,   291,   292,   278,   279,     0,   280,   281,   282,
     283,   284,   285,   286,   287,   288,   289,   290,   291,   292
};

static const yytype_int16 yycheck[] =
{
      36,    37,   128,   129,     4,     4,    39,   133,     8,     5,
       3,     4,     5,     6,     5,     8,     3,     4,     9,     5,
       7,     8,     4,     5,     5,     3,     4,     5,     6,     7,
       8,     4,   207,     3,     4,     3,     4,     7,     8,     5,
       8,     4,     5,     4,     7,     8,     7,     8,   223,     5,
       4,     5,     4,     5,     6,     7,     8,     4,     4,   223,
     224,     7,     8,   217,   218,   219,   220,     8,     5,     4,
     223,   224,   198,    14,    15,   108,   109,     3,     4,     5,
       6,     5,     8,     4,    25,    26,     4,    28,     5,     6,
     463,   464,     3,     3,   130,     3,   132,     3,     4,     5,
       6,     7,     8,     3,     3,     8,   139,   140,     3,     3,
      13,    14,    15,     3,    17,    18,    19,    20,    21,    22,
      23,    24,    25,    26,    27,    28,     3,     3,    31,    32,
      33,    34,    35,     0,   207,   207,    39,    40,    41,    42,
      43,    44,   207,   207,   207,   181,   182,   183,   184,     4,
     207,   207,     3,     5,     4,     4,     3,    60,    61,   211,
     211,   211,   211,    66,    67,   208,   209,   210,   211,   212,
     213,   214,   215,   216,   217,   218,   219,   220,     3,   120,
       3,    84,     3,    86,   205,   206,   225,   208,   209,   210,
     211,   212,   213,   214,   215,   216,   217,   218,   219,   220,
       3,     3,   223,   224,     3,     3,   199,   200,     3,   112,
       3,   207,   115,     5,     3,     5,     4,   120,   211,   212,
     213,   214,   215,     5,     4,     4,     4,   227,   221,   222,
     222,   227,     5,   226,   227,     5,   214,   215,   216,     3,
     227,   219,   278,   279,   280,   281,   282,   283,   284,   227,
     286,   287,   288,   289,   290,   291,   292,   227,     3,   227,
     293,   227,   214,   215,   227,     5,   227,     5,   171,   172,
     207,   227,   222,   199,   200,   227,   225,     3,     4,     5,
       6,   227,     8,   222,   222,   211,   222,   213,   214,   215,
     227,   222,   222,   199,   200,   221,   222,   214,   215,   222,
     226,   227,   428,   222,   211,   220,   222,   213,   214,   215,
     222,     5,     5,   222,   211,   221,   222,   222,   222,     5,
     226,   227,   211,   211,     3,     4,     5,     6,   211,     8,
     222,   367,   368,   369,   370,   371,   372,   373,   374,   375,
     376,   377,   378,   379,   380,   381,   382,   383,   384,   385,
     386,   387,   388,   389,   390,   391,   392,   393,   394,   395,
     396,   397,   398,   399,   400,   401,   402,   403,   404,   405,
     406,   407,   408,   409,   410,   411,   412,   413,   414,   415,
     416,   417,   418,   419,   420,   421,   422,   423,   424,   425,
     426,   427,   222,   222,   222,   222,   222,   211,   434,   435,
     222,   211,   438,   222,   440,   209,   210,   211,   212,   213,
     214,   215,   216,   217,   218,   219,   220,   222,     4,   360,
     361,   362,   363,   364,   211,   222,   222,   211,   222,     4,
     211,   222,     4,   559,   467,   210,   211,   212,   213,   214,
     215,   216,   217,   218,   219,   220,   222,   388,   389,   211,
     222,   222,   211,   222,   222,   222,   222,   360,   361,   362,
     363,   364,   365,   366,   222,   222,   222,   222,   222,   222,
     222,   222,   222,   199,   200,   222,   222,   418,   419,   420,
     421,   422,     5,   222,   211,   388,   389,   213,   214,   215,
     222,   222,   222,   222,   222,   221,   222,   222,   222,   222,
     226,   227,   210,   211,   212,   213,   214,   215,   216,   217,
     218,   219,   220,   222,     5,   418,   419,   420,   421,   422,
     199,   200,   222,   222,   560,   222,   222,     3,     4,     5,
       6,   222,     8,   222,   213,   214,   215,   222,   222,   222,
     222,   222,   221,   222,   222,   222,   211,   226,   227,   211,
     211,   587,   211,   589,   211,   591,   211,   593,   211,   595,
     463,   464,     5,     5,     5,     3,     4,     5,     6,   605,
       8,     5,     5,     5,     5,   611,     5,     5,   614,     5,
       5,     5,     5,     5,     5,   621,   224,   223,   624,   224,
     223,   627,   628,   629,   630,   631,   224,   223,   623,   224,
     223,    -1,   224,   639,   640,   641,   642,   643,   644,   223,
     223,   223,    -1,   224,   650,   651,   652,   653,   654,   223,
     656,   224,   658,   223,   660,   224,   662,   224,   664,   224,
     224,   224,    17,    18,    19,    20,    21,    22,    23,    24,
     223,   223,    27,   224,   224,   223,    31,    32,    -1,    34,
      -1,    -1,    -1,    -1,    39,    40,    41,    42,    43,    44,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    60,    61,    -1,    -1,    -1,
      -1,    66,    67,    -1,   720,    -1,    -1,    -1,    -1,   725,
     726,   727,   728,    -1,   730,   731,    -1,   733,   734,    84,
      -1,    86,    -1,    -1,    -1,    -1,   742,   743,   744,   745,
      -1,    -1,   748,    -1,   655,   751,   657,    -1,   659,    -1,
     661,    -1,   663,   199,   200,    -1,    -1,   112,    -1,    -1,
     115,    -1,    -1,    -1,    -1,    -1,    -1,   213,   214,    -1,
      -1,    -1,    -1,    -1,    -1,   221,   222,   223,    -1,    -1,
     226,   227,   655,    -1,   657,    -1,   659,    -1,   661,    -1,
     663,   199,   200,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   213,   214,    -1,    -1,    -1,
      -1,    -1,    -1,   221,   222,    -1,    -1,   172,   226,   227,
     114,   115,   116,   117,   118,   119,   120,   121,   122,   123,
     124,   125,   126,   127,   128,   129,   130,   131,   132,   133,
     134,   135,   136,   137,   138,   139,   140,   141,   142,   143,
     144,   145,   146,   147,   148,   149,   150,   151,   152,   153,
     154,   155,   156,   157,   158,   159,   160,   161,   162,   163,
     164,   165,   166,   167,   168,   169,   170,   171,   172,   173,
     174,   175,   176,   177,   178,   179,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   187,   188,   189,   190,   191,   192,    -1,
      -1,    -1,    -1,    -1,    -1,   199,   200,    -1,   202,   203,
     204,   114,   115,   116,   117,   118,   119,   120,   121,   122,
     123,   124,   125,   126,   127,   128,   129,   130,   131,   132,
     133,   134,   135,   136,   137,   138,   139,   140,   141,   142,
     143,   144,   145,   146,   147,   148,   149,   150,   151,   152,
     153,   154,   155,   156,   157,   158,   159,   160,   161,   162,
     163,   164,   165,   166,   167,   168,   169,   170,   171,   172,
     173,   174,   175,   176,   177,   178,   179,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   187,   188,   189,   190,   191,   192,
      -1,    -1,    -1,    -1,    -1,    -1,   199,   200,    -1,   202,
     203,   204,    -1,    -1,    -1,   360,   361,   362,   363,   364,
     365,   366,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,     0,     1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   388,   389,    12,    13,    14,    15,    16,
      17,    18,    19,    20,    21,    22,    23,    24,    25,    26,
      27,    28,    29,    30,    31,    32,    33,    34,    35,    36,
      37,    38,    39,    40,    41,    42,    43,    44,    45,    46,
      47,    48,    49,    50,    51,    52,    53,    54,    55,    56,
      57,    58,    59,    60,    61,    62,    63,    64,    65,    66,
      67,    68,    69,    70,    71,    72,    73,    74,    75,    76,
      77,    -1,    78,    79,    80,    81,    82,    83,    84,    85,
      86,    87,    88,    89,    90,    91,    92,    93,    94,    95,
      96,    97,    98,    99,   100,   101,   102,   103,   104,   105,
     106,   107,   108,   109,   110,   111,   205,   206,    -1,   208,
     209,   210,   211,   212,   213,   214,   215,   216,   217,   218,
     219,   220,    -1,    -1,   223,   224,   205,   206,    -1,   208,
     209,   210,   211,   212,   213,   214,   215,   216,   217,   218,
     219,   220,    -1,    -1,   223,   224,   205,   206,    -1,   208,
     209,   210,   211,   212,   213,   214,   215,   216,   217,   218,
     219,   220,    -1,   205,   206,   224,   208,   209,   210,   211,
     212,   213,   214,   215,   216,   217,   218,   219,   220,    -1,
     205,   206,   224,   208,   209,   210,   211,   212,   213,   214,
     215,   216,   217,   218,   219,   220,    -1,   205,   206,   224,
     208,   209,   210,   211,   212,   213,   214,   215,   216,   217,
     218,   219,   220,    -1,   205,   206,   224,   208,   209,   210,
     211,   212,   213,   214,   215,   216,   217,   218,   219,   220,
      -1,   205,   206,   224,   208,   209,   210,   211,   212,   213,
     214,   215,   216,   217,   218,   219,   220,    -1,   205,   206,
     224,   208,   209,   210,   211,   212,   213,   214,   215,   216,
     217,   218,   219,   220,    -1,   205,   206,   224,   208,   209,
     210,   211,   212,   213,   214,   215,   216,   217,   218,   219,
     220,    -1,   205,   206,   224,   208,   209,   210,   211,   212,
     213,   214,   215,   216,   217,   218,   219,   220,    -1,   205,
     206,   224,   208,   209,   210,   211,   212,   213,   214,   215,
     216,   217,   218,   219,   220,    -1,   205,   206,   224,   208,
     209,   210,   211,   212,   213,   214,   215,   216,   217,   218,
     219,   220,    -1,   205,   206,   224,   208,   209,   210,   211,
     212,   213,   214,   215,   216,   217,   218,   219,   220,    -1,
     205,   206,   224,   208,   209,   210,   211,   212,   213,   214,
     215,   216,   217,   218,   219,   220,    -1,   205,   206,   224,
     208,   209,   210,   211,   212,   213,   214,   215,   216,   217,
     218,   219,   220,    -1,   205,   206,   224,   208,   209,   210,
     211,   212,   213,   214,   215,   216,   217,   218,   219,   220,
      -1,   205,   206,   224,   208,   209,   210,   211,   212,   213,
     214,   215,   216,   217,   218,   219,   220,    -1,   205,   206,
     224,   208,   209,   210,   211,   212,   213,   214,   215,   216,
     217,   218,   219,   220,    -1,   205,   206,   224,   208,   209,
     210,   211,   212,   213,   214,   215,   216,   217,   218,   219,
     220,    -1,   205,   206,   224,   208,   209,   210,   211,   212,
     213,   214,   215,   216,   217,   218,   219,   220,    -1,   205,
     206,   224,   208,   209,   210,   211,   212,   213,   214,   215,
     216,   217,   218,   219,   220,    -1,   205,   206,   224,   208,
     209,   210,   211,   212,   213,   214,   215,   216,   217,   218,
     219,   220,    -1,   205,   206,   224,   208,   209,   210,   211,
     212,   213,   214,   215,   216,   217,   218,   219,   220,    -1,
     205,   206,   224,   208,   209,   210,   211,   212,   213,   214,
     215,   216,   217,   218,   219,   220,    -1,   205,   206,   224,
     208,   209,   210,   211,   212,   213,   214,   215,   216,   217,
     218,   219,   220,    -1,   205,   206,   224,   208,   209,   210,
     211,   212,   213,   214,   215,   216,   217,   218,   219,   220,
      -1,   205,   206,   224,   208,   209,   210,   211,   212,   213,
     214,   215,   216,   217,   218,   219,   220,    -1,   205,   206,
     224,   208,   209,   210,   211,   212,   213,   214,   215,   216,
     217,   218,   219,   220,    -1,   205,   206,   224,   208,   209,
     210,   211,   212,   213,   214,   215,   216,   217,   218,   219,
     220,    -1,   205,   206,   224,   208,   209,   210,   211,   212,
     213,   214,   215,   216,   217,   218,   219,   220,    -1,   205,
     206,   224,   208,   209,   210,   211,   212,   213,   214,   215,
     216,   217,   218,   219,   220,    -1,   205,   206,   224,   208,
     209,   210,   211,   212,   213,   214,   215,   216,   217,   218,
     219,   220,    -1,   205,   206,   224,   208,   209,   210,   211,
     212,   213,   214,   215,   216,   217,   218,   219,   220,    -1,
     205,   206,   224,   208,   209,   210,   211,   212,   213,   214,
     215,   216,   217,   218,   219,   220,    -1,   205,   206,   224,
     208,   209,   210,   211,   212,   213,   214,   215,   216,   217,
     218,   219,   220,    -1,   205,   206,   224,   208,   209,   210,
     211,   212,   213,   214,   215,   216,   217,   218,   219,   220,
      -1,   205,   206,   224,   208,   209,   210,   211,   212,   213,
     214,   215,   216,   217,   218,   219,   220,   205,   206,   223,
     208,   209,   210,   211,   212,   213,   214,   215,   216,   217,
     218,   219,   220,   205,   206,   223,   208,   209,   210,   211,
     212,   213,   214,   215,   216,   217,   218,   219,   220,   205,
     206,   223,   208,   209,   210,   211,   212,   213,   214,   215,
     216,   217,   218,   219,   220,   205,   206,   223,   208,   209,
     210,   211,   212,   213,   214,   215,   216,   217,   218,   219,
     220,   205,   206,   223,   208,   209,   210,   211,   212,   213,
     214,   215,   216,   217,   218,   219,   220,   205,   206,   223,
     208,   209,   210,   211,   212,   213,   214,   215,   216,   217,
     218,   219,   220,   205,   206,   223,   208,   209,   210,   211,
     212,   213,   214,   215,   216,   217,   218,   219,   220,   205,
     206,   223,   208,   209,   210,   211,   212,   213,   214,   215,
     216,   217,   218,   219,   220,   205,   206,   223,   208,   209,
     210,   211,   212,   213,   214,   215,   216,   217,   218,   219,
     220,   205,   206,   223,   208,   209,   210,   211,   212,   213,
     214,   215,   216,   217,   218,   219,   220,   205,   206,   223,
     208,   209,   210,   211,   212,   213,   214,   215,   216,   217,
     218,   219,   220,   205,   206,   223,   208,   209,   210,   211,
     212,   213,   214,   215,   216,   217,   218,   219,   220,   205,
     206,   223,   208,   209,   210,   211,   212,   213,   214,   215,
     216,   217,   218,   219,   220,   205,   206,   223,   208,   209,
     210,   211,   212,   213,   214,   215,   216,   217,   218,   219,
     220,   205,   206,   223,   208,   209,   210,   211,   212,   213,
     214,   215,   216,   217,   218,   219,   220,   205,   206,   223,
     208,   209,   210,   211,   212,   213,   214,   215,   216,   217,
     218,   219,   220,   205,   206,   223,   208,   209,   210,   211,
     212,   213,   214,   215,   216,   217,   218,   219,   220,   205,
     206,   223,   208,   209,   210,   211,   212,   213,   214,   215,
     216,   217,   218,   219,   220,   205,   206,   223,   208,   209,
     210,   211,   212,   213,   214,   215,   216,   217,   218,   219,
     220,   205,   206,   223,   208,   209,   210,   211,   212,   213,
     214,   215,   216,   217,   218,   219,   220,   205,   206,   223,
     208,   209,   210,   211,   212,   213,   214,   215,   216,   217,
     218,   219,   220,   205,   206,   223,   208,   209,   210,   211,
     212,   213,   214,   215,   216,   217,   218,   219,   220,   205,
     206,   223,   208,   209,   210,   211,   212,   213,   214,   215,
     216,   217,   218,   219,   220,   205,   206,   223,   208,   209,
     210,   211,   212,   213,   214,   215,   216,   217,   218,   219,
     220,   205,   206,   223,   208,   209,   210,   211,   212,   213,
     214,   215,   216,   217,   218,   219,   220,   205,   206,   223,
     208,   209,   210,   211,   212,   213,   214,   215,   216,   217,
     218,   219,   220,   205,   206,   223,   208,   209,   210,   211,
     212,   213,   214,   215,   216,   217,   218,   219,   220,   205,
     206,   223,   208,   209,   210,   211,   212,   213,   214,   215,
     216,   217,   218,   219,   220,   205,   206,   223,   208,   209,
     210,   211,   212,   213,   214,   215,   216,   217,   218,   219,
     220,   205,   206,   223,   208,   209,   210,   211,   212,   213,
     214,   215,   216,   217,   218,   219,   220,   205,   206,   223,
     208,   209,   210,   211,   212,   213,   214,   215,   216,   217,
     218,   219,   220,   205,   206,   223,   208,   209,   210,   211,
     212,   213,   214,   215,   216,   217,   218,   219,   220,   205,
     206,   223,   208,   209,   210,   211,   212,   213,   214,   215,
     216,   217,   218,   219,   220,   205,   206,   223,   208,   209,
     210,   211,   212,   213,   214,   215,   216,   217,   218,   219,
     220,   205,   206,   223,   208,   209,   210,   211,   212,   213,
     214,   215,   216,   217,   218,   219,   220,   205,   206,   223,
     208,   209,   210,   211,   212,   213,   214,   215,   216,   217,
     218,   219,   220,   205,   206,   223,   208,   209,   210,   211,
     212,   213,   214,   215,   216,   217,   218,   219,   220,   205,
     206,   223,   208,   209,   210,   211,   212,   213,   214,   215,
     216,   217,   218,   219,   220,   205,   206,   223,   208,   209,
     210,   211,   212,   213,   214,   215,   216,   217,   218,   219,
     220,   205,   206,   223,   208,   209,   210,   211,   212,   213,
     214,   215,   216,   217,   218,   219,   220,   205,   206,   223,
     208,   209,   210,   211,   212,   213,   214,   215,   216,   217,
     218,   219,   220,   205,   206,   223,   208,   209,   210,   211,
     212,   213,   214,   215,   216,   217,   218,   219,   220,   205,
     206,   223,   208,   209,   210,   211,   212,   213,   214,   215,
     216,   217,   218,   219,   220,   205,   206,   223,   208,   209,
     210,   211,   212,   213,   214,   215,   216,   217,   218,   219,
     220,   205,   206,   223,   208,   209,   210,   211,   212,   213,
     214,   215,   216,   217,   218,   219,   220,   205,   206,   223,
     208,   209,   210,   211,   212,   213,   214,   215,   216,   217,
     218,   219,   220,   205,   206,   223,   208,   209,   210,   211,
     212,   213,   214,   215,   216,   217,   218,   219,   220,   205,
     206,   223,   208,   209,   210,   211,   212,   213,   214,   215,
     216,   217,   218,   219,   220,   205,   206,   223,   208,   209,
     210,   211,   212,   213,   214,   215,   216,   217,   218,   219,
     220,   205,   206,   223,   208,   209,   210,   211,   212,   213,
     214,   215,   216,   217,   218,   219,   220,   205,   206,   223,
     208,   209,   210,   211,   212,   213,   214,   215,   216,   217,
     218,   219,   220,   205,   206,   223,   208,   209,   210,   211,
     212,   213,   214,   215,   216,   217,   218,   219,   220,   205,
     206,   223,   208,   209,   210,   211,   212,   213,   214,   215,
     216,   217,   218,   219,   220,   205,   206,   223,   208,   209,
     210,   211,   212,   213,   214,   215,   216,   217,   218,   219,
     220,   205,   206,   223,   208,   209,   210,   211,   212,   213,
     214,   215,   216,   217,   218,   219,   220,   205,   206,   223,
     208,   209,   210,   211,   212,   213,   214,   215,   216,   217,
     218,   219,   220,   205,   206,   223,   208,   209,   210,   211,
     212,   213,   214,   215,   216,   217,   218,   219,   220,   205,
     206,   223,   208,   209,   210,   211,   212,   213,   214,   215,
     216,   217,   218,   219,   220,   205,   206,   223,   208,   209,
     210,   211,   212,   213,   214,   215,   216,   217,   218,   219,
     220,   205,   206,   223,   208,   209,   210,   211,   212,   213,
     214,   215,   216,   217,   218,   219,   220,   205,   206,   223,
     208,   209,   210,   211,   212,   213,   214,   215,   216,   217,
     218,   219,   220,   205,   206,   223,   208,   209,   210,   211,
     212,   213,   214,   215,   216,   217,   218,   219,   220,   205,
     206,   223,   208,   209,   210,   211,   212,   213,   214,   215,
     216,   217,   218,   219,   220,   205,   206,   223,   208,   209,
     210,   211,   212,   213,   214,   215,   216,   217,   218,   219,
     220,   205,   206,   223,   208,   209,   210,   211,   212,   213,
     214,   215,   216,   217,   218,   219,   220,   205,   206,   223,
     208,   209,   210,   211,   212,   213,   214,   215,   216,   217,
     218,   219,   220,   205,   206,   223,   208,   209,   210,   211,
     212,   213,   214,   215,   216,   217,   218,   219,   220,   205,
     206,   223,   208,   209,   210,   211,   212,   213,   214,   215,
     216,   217,   218,   219,   220,    -1,    -1,   223,   205,   206,
     207,   208,   209,   210,   211,   212,   213,   214,   215,   216,
     217,   218,   219,   220,   205,   206,    -1,   208,   209,   210,
     211,   212,   213,   214,   215,   216,   217,   218,   219,   220
};

  /* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
     symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,     1,    12,    13,    14,    15,    16,    17,    18,    19,
      20,    21,    22,    23,    24,    25,    26,    27,    28,    29,
      30,    31,    32,    33,    34,    35,    36,    37,    38,    39,
      40,    41,    42,    43,    44,    45,    46,    47,    48,    49,
      50,    51,    52,    53,    54,    55,    56,    57,    58,    59,
      60,    61,    62,    63,    64,    65,    66,    67,    68,    69,
      70,    71,    72,    73,    74,    75,    76,    77,   229,     4,
       5,     4,     5,     5,     5,     4,     4,     4,     5,     7,
       8,   227,   233,   234,     4,     4,     5,     3,     4,   233,
     234,   235,   233,   233,     4,   235,   235,   235,   235,   235,
     235,   235,   235,   233,   233,     3,     5,     6,   214,   215,
     216,   219,   235,   236,   233,     3,   235,   235,     3,   234,
     237,   235,   234,     3,     5,     6,   199,   200,   213,   214,
     215,   221,   222,   226,   230,   231,   234,   231,     3,   235,
     236,   235,   235,   235,   235,   235,     4,     3,     3,     3,
       3,     3,     3,     3,     3,   235,   235,     3,   238,   235,
     235,     0,   207,   207,   207,   207,     5,   207,   227,   207,
       4,   207,   235,   207,     4,   235,     3,     4,     4,     5,
       3,   211,   211,   211,   211,     3,   236,   236,     3,     3,
     235,   235,   233,   234,   225,   230,   230,   231,   114,   115,
     116,   117,   118,   119,   120,   121,   122,   123,   124,   125,
     126,   127,   128,   129,   130,   131,   132,   133,   134,   135,
     136,   137,   138,   139,   140,   141,   142,   143,   144,   145,
     146,   147,   148,   149,   150,   151,   152,   153,   154,   155,
     156,   157,   158,   159,   160,   161,   162,   163,   164,   165,
     166,   167,   168,   169,   170,   171,   172,   173,   174,   175,
     176,   177,   178,   179,   187,   188,   189,   190,   191,   192,
     199,   200,   202,   203,   204,   221,   231,   230,   205,   206,
     208,   209,   210,   211,   212,   213,   214,   215,   216,   217,
     218,   219,   220,   236,   236,     3,     3,     3,     3,     3,
       3,     5,    78,    79,    80,    81,    82,    83,    84,    85,
      86,    87,    88,    89,    90,    91,    92,    93,    94,    95,
      96,    97,    98,    99,   100,   101,   102,   103,   104,   105,
     106,   107,   108,   109,   110,   111,   239,     3,     5,     5,
       4,     4,     4,     5,     5,     5,   227,   234,   235,     4,
     207,     3,     3,     5,   231,   231,   231,   231,     5,   230,
     222,   222,   222,   222,   222,   222,   222,   222,   222,   222,
     222,   222,   222,   222,   222,   222,   222,   222,   222,   222,
     222,   222,   222,   222,   222,   222,   222,   222,   222,   222,
     222,   222,   222,   222,   222,   222,   222,   222,   222,   222,
     222,   222,   222,   222,   222,   222,   222,   222,   222,   222,
     222,   222,   222,   222,   222,   222,   222,   222,   222,   222,
     222,   222,   222,   222,   222,   222,   222,   222,   114,   223,
     231,   231,   231,   231,   211,   212,   231,   231,   211,   231,
     211,   231,   231,   231,   231,   231,   231,   231,   236,     5,
     211,   211,   211,   211,   211,   211,   211,   211,   211,   211,
     211,   211,   211,   211,   211,   211,   211,   211,   211,     5,
       4,     4,     4,     5,   225,   233,   235,   233,   235,   233,
     235,   233,   235,   233,   235,   235,   235,   231,   231,   231,
     231,   231,   231,   231,   231,   231,   231,   231,   231,   231,
     231,   231,   231,   231,   231,   231,   231,   231,   231,   233,
     234,   235,   231,   233,   235,   231,   231,   231,   231,   231,
     231,   231,   231,   231,   231,   231,   231,   231,   231,   231,
     231,   231,   231,   231,   231,   231,   231,   231,   231,   231,
     231,   231,   231,   231,   233,   234,   231,   233,   231,   233,
     231,   233,   231,   233,   231,   231,   231,   231,   231,   223,
     207,   231,   231,   231,   231,     5,     5,     5,     5,     5,
       5,     5,     5,     5,     5,     5,     5,     5,     5,   237,
     237,     5,     5,   236,     5,     9,     5,   224,   223,   224,
     223,   224,   223,   224,   223,   224,   223,   223,   223,   223,
     223,   223,   223,   223,   223,   224,   223,   223,   223,   223,
     223,   224,   223,   223,   224,   223,   223,   223,   223,   223,
     224,   224,   223,   224,   224,   223,   223,   224,   224,   224,
     224,   224,   223,   223,   223,   223,   223,   223,   223,   224,
     224,   224,   224,   224,   224,   223,   223,   223,   223,   223,
     224,   224,   224,   224,   224,   224,   224,   224,   224,   224,
     224,   224,   224,   224,   224,   223,   223,   223,   223,   223,
     230,   231,   231,   231,   231,   231,   231,   231,   231,   231,
     231,   232,   231,   232,   231,   231,   231,   231,   231,   231,
     231,   231,   231,   231,   231,   231,   231,   231,   231,   231,
     231,   233,   231,   233,   231,   233,   231,   233,   231,   233,
     231,   223,   223,   223,   223,   223,   223,   223,   223,   223,
     224,   223,   223,   223,   223,   224,   224,   224,   224,   223,
     224,   224,   223,   224,   224,   223,   223,   223,   223,   223,
     223,   223,   224,   224,   224,   224,   223,   223,   224,   223,
     223,   224,   231,   231,   231,   231,   231,   231,   231,   231,
     231,   231,   231,   231,   231,   231,   231,   223,   223,   223,
     223,   223,   223,   223,   223,   223,   223,   223,   223,   223,
     223
};

  /* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,   228,   229,   229,   229,   229,   229,   229,   229,   229,
     229,   229,   229,   229,   229,   229,   229,   229,   229,   229,
     229,   229,   229,   229,   229,   229,   229,   229,   229,   229,
     229,   229,   229,   229,   229,   229,   229,   229,   229,   229,
     229,   229,   229,   229,   229,   229,   229,   229,   229,   229,
     229,   229,   229,   229,   229,   229,   229,   229,   229,   229,
     229,   229,   229,   229,   229,   229,   229,   229,   229,   229,
     229,   229,   229,   229,   229,   229,   229,   229,   229,   229,
     229,   229,   229,   229,   229,   229,   229,   229,   229,   229,
     229,   229,   229,   229,   229,   230,   230,   230,   230,   230,
     230,   230,   230,   230,   230,   230,   230,   230,   230,   230,
     230,   230,   230,   230,   230,   230,   230,   230,   230,   230,
     230,   230,   230,   230,   230,   230,   230,   230,   230,   230,
     230,   230,   230,   230,   230,   230,   230,   230,   230,   230,
     230,   230,   230,   230,   230,   230,   230,   230,   230,   230,
     230,   230,   230,   230,   230,   230,   230,   230,   230,   230,
     230,   230,   230,   230,   230,   230,   230,   230,   230,   230,
     230,   230,   230,   230,   230,   230,   230,   230,   230,   230,
     230,   230,   230,   230,   230,   230,   230,   230,   230,   230,
     230,   230,   230,   230,   230,   230,   230,   230,   230,   230,
     230,   231,   231,   231,   231,   231,   231,   231,   231,   231,
     231,   231,   231,   231,   231,   231,   231,   231,   231,   231,
     231,   232,   232,   233,   233,   234,   234,   234,   234,   234,
     235,   235,   236,   236,   236,   236,   237,   237,   238,   238,
     239,   239,   239,   239,   239,   239,   239,   239,   239,   239,
     239,   239,   239,   239,   239,   239,   239,   239,   239,   239,
     239,   239,   239,   239,   239,   239,   239,   239,   239,   239,
     239,   239,   239,   239,   239,   239,   239,   239,   239,   239,
     239,   239,   239,   239,   239,   239,   239,   239,   239,   239,
     239
};

  /* YYR2[YYN] -- Number of symbols on the right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     4,     2,     3,     4,     4,     4,     2,     2,
       2,     5,     3,     3,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     4,     4,     4,     4,     3,     3,     4,
       4,     3,     1,     2,     4,     2,     4,     2,     3,     5,
       5,     2,     4,     2,     3,     2,     2,     2,     3,     3,
       2,     1,     2,     2,     3,     3,     3,     3,     2,     2,
       2,     3,     3,     2,     2,     2,     1,     5,     1,     1,
       5,     3,     3,     3,     2,     3,     3,     2,     2,     1,
       1,     1,     1,     1,     1,     1,     2,     2,     2,     2,
       2,     2,     2,     0,     1,     1,     3,     5,     5,     7,
       5,     7,     5,     7,     5,     7,     5,     7,     5,     7,
       7,     5,     7,     7,     5,     5,     5,     5,     5,     5,
       5,     5,     7,     5,     5,     5,     5,     5,     7,     5,
       5,     7,     5,     5,     5,     5,     5,     5,     7,     9,
       9,     9,     9,     5,     5,     5,     5,     5,     5,     2,
       9,     5,     9,     5,     5,     7,     5,     7,     7,     5,
       5,     5,     7,     7,     9,     7,     7,     9,     9,     9,
       9,     7,     7,     9,     7,     7,     7,     7,     9,     9,
       3,     2,     1,     1,     2,     1,     2,     2,     5,     2,
       2,     2,     2,     5,     5,     5,     5,     2,     1,     2,
       1,     3,     3,     3,     3,     3,     2,     3,     1,     5,
       3,     3,     3,     3,     3,     3,     4,     4,     4,     4,
       3,     1,     3,     3,     1,     2,     3,     3,     4,     1,
       1,     1,     1,     1,     2,     2,     1,     1,     0,     2,
       3,     1,     1,     3,     1,     1,     3,     1,     1,     3,
       1,     1,     3,     1,     1,     3,     1,     1,     3,     1,
       1,     3,     1,     1,     3,     1,     1,     3,     1,     1,
       3,     1,     1,     3,     1,     1,     3,     1,     1,     3,
       1,     1,     3,     3,     3,     1,     3,     1,     3,     1,
       3
};


#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)
#define YYEMPTY         (-2)
#define YYEOF           0

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                  \
do                                                              \
  if (yychar == YYEMPTY)                                        \
    {                                                           \
      yychar = (Token);                                         \
      yylval = (Value);                                         \
      YYPOPSTACK (yylen);                                       \
      yystate = *yyssp;                                         \
      goto yybackup;                                            \
    }                                                           \
  else                                                          \
    {                                                           \
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;                                                  \
    }                                                           \
while (0)

/* Error token number */
#define YYTERROR        1
#define YYERRCODE       256



/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)

/* This macro is provided for backward compatibility. */
#ifndef YY_LOCATION_PRINT
# define YY_LOCATION_PRINT(File, Loc) ((void) 0)
#endif


# define YY_SYMBOL_PRINT(Title, Type, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Type, Value); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*----------------------------------------.
| Print this symbol's value on YYOUTPUT.  |
`----------------------------------------*/

static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
{
  FILE *yyo = yyoutput;
  YYUSE (yyo);
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# endif
  YYUSE (yytype);
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
{
  YYFPRINTF (yyoutput, "%s %s (",
             yytype < YYNTOKENS ? "token" : "nterm", yytname[yytype]);

  yy_symbol_value_print (yyoutput, yytype, yyvaluep);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yytype_int16 *yyssp, YYSTYPE *yyvsp, int yyrule)
{
  unsigned long int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       yystos[yyssp[yyi + 1 - yynrhs]],
                       &(yyvsp[(yyi + 1) - (yynrhs)])
                                              );
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, Rule); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
static YYSIZE_T
yystrlen (const char *yystr)
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
yystpcpy (char *yydest, const char *yysrc)
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
        switch (*++yyp)
          {
          case '\'':
          case ',':
            goto do_not_strip_quotes;

          case '\\':
            if (*++yyp != '\\')
              goto do_not_strip_quotes;
            /* Fall through.  */
          default:
            if (yyres)
              yyres[yyn] = *yyp;
            yyn++;
            break;

          case '"':
            if (yyres)
              yyres[yyn] = '\0';
            return yyn;
          }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return 1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return 2 if the
   required number of bytes is too large to store.  */
static int
yysyntax_error (YYSIZE_T *yymsg_alloc, char **yymsg,
                yytype_int16 *yyssp, int yytoken)
{
  YYSIZE_T yysize0 = yytnamerr (YY_NULLPTR, yytname[yytoken]);
  YYSIZE_T yysize = yysize0;
  enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
  /* Internationalized format string. */
  const char *yyformat = YY_NULLPTR;
  /* Arguments of yyformat. */
  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
  /* Number of reported tokens (one for the "unexpected", one per
     "expected"). */
  int yycount = 0;

  /* There are many possibilities here to consider:
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yytoken != YYEMPTY)
    {
      int yyn = yypact[*yyssp];
      yyarg[yycount++] = yytname[yytoken];
      if (!yypact_value_is_default (yyn))
        {
          /* Start YYX at -YYN if negative to avoid negative indexes in
             YYCHECK.  In other words, skip the first -YYN actions for
             this state because they are default actions.  */
          int yyxbegin = yyn < 0 ? -yyn : 0;
          /* Stay within bounds of both yycheck and yytname.  */
          int yychecklim = YYLAST - yyn + 1;
          int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
          int yyx;

          for (yyx = yyxbegin; yyx < yyxend; ++yyx)
            if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR
                && !yytable_value_is_error (yytable[yyx + yyn]))
              {
                if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                  {
                    yycount = 1;
                    yysize = yysize0;
                    break;
                  }
                yyarg[yycount++] = yytname[yyx];
                {
                  YYSIZE_T yysize1 = yysize + yytnamerr (YY_NULLPTR, yytname[yyx]);
                  if (! (yysize <= yysize1
                         && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
                    return 2;
                  yysize = yysize1;
                }
              }
        }
    }

  switch (yycount)
    {
# define YYCASE_(N, S)                      \
      case N:                               \
        yyformat = S;                       \
      break
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
# undef YYCASE_
    }

  {
    YYSIZE_T yysize1 = yysize + yystrlen (yyformat);
    if (! (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
      return 2;
    yysize = yysize1;
  }

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return 1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp += yytnamerr (yyp, yyarg[yyi++]);
          yyformat += 2;
        }
      else
        {
          yyp++;
          yyformat++;
        }
  }
  return 0;
}
#endif /* YYERROR_VERBOSE */

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
{
  YYUSE (yyvaluep);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YYUSE (yytype);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}




/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;
/* Number of syntax errors so far.  */
int yynerrs;


/*----------.
| yyparse.  |
`----------*/

int
yyparse (void)
{
    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       'yyss': related to states.
       'yyvs': related to semantic values.

       Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken = 0;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yyssp = yyss = yyssa;
  yyvsp = yyvs = yyvsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */
  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        YYSTYPE *yyvs1 = yyvs;
        yytype_int16 *yyss1 = yyss;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * sizeof (*yyssp),
                    &yyvs1, yysize * sizeof (*yyvsp),
                    &yystacksize);

        yyss = yyss1;
        yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yytype_int16 *yyss1 = yyss;
        union yyalloc *yyptr =
          (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
        if (! yyptr)
          goto yyexhaustedlab;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
                  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = yylex ();
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 2:
#line 383 "gram.y" /* yacc.c:1646  */
    { let((yyvsp[-2].rval).left.vp, (yyvsp[0].enode));
                                  }
#line 2628 "y.tab.c" /* yacc.c:1646  */
    break;

  case 3:
#line 385 "gram.y" /* yacc.c:1646  */
    {
                                  char det[BUFFERSIZE] = "";
                                  struct ent * e = (yyvsp[0].ent).vp;

                                  sprintf(det + strlen(det), "row: %d\n", e->row);
                                  sprintf(det + strlen(det), "col: %d\n", e->col);
                                  sprintf(det + strlen(det), "cellerror: %d\n"   , e->cellerror);
                                  sprintf(det + strlen(det), "flags:\n");
                                  sprintf(det + strlen(det), "is_valid: %d\n"    , e->flags & is_valid );
                                  sprintf(det + strlen(det), "is_deleted: %d\n"  , e->flags & is_deleted);
                                  sprintf(det + strlen(det), "is_changed: %d\n"  , e->flags & is_changed);
                                  sprintf(det + strlen(det), "is_strexpr: %d\n"  , e->flags & is_strexpr);
                                  sprintf(det + strlen(det), "is_leftflush: %d\n", e->flags & is_leftflush);
                                  sprintf(det + strlen(det), "is_locked: %d\n"   , e->flags & is_locked);
                                  sprintf(det + strlen(det), "is_label: %d\n"    , e->flags & is_label);
                                  sprintf(det + strlen(det), "iscleared: %d\n"   , e->flags & iscleared);
                                  sprintf(det + strlen(det), "may_sync: %d\n"    , e->flags & may_sync);
                                  ui_show_text((char *) &det);
                                  }
#line 2652 "y.tab.c" /* yacc.c:1646  */
    break;

  case 4:
#line 405 "gram.y" /* yacc.c:1646  */
    {
                                  // TODO get this code out of gram.y
                                  extern graphADT graph;
                                  #ifdef UNDO
                                  create_undo_action();
                                  copy_to_undostruct((yyvsp[-1].rval).left.vp->row, (yyvsp[-1].rval).left.vp->col, (yyvsp[-1].rval).left.vp->row, (yyvsp[-1].rval).left.vp->col, 'd');
                                  // here we save in undostruct, all the ents that depends on the deleted one (before change)
                                  extern struct ent_ptr * deps;
                                  int i, n = 0;
                                  ents_that_depends_on_range((yyvsp[-1].rval).left.vp->row, (yyvsp[-1].rval).left.vp->col, (yyvsp[-1].rval).left.vp->row, (yyvsp[-1].rval).left.vp->col);
                                  if (deps != NULL) {
                                      for (i = 0, n = deps->vf; i < n; i++)
                                          copy_to_undostruct(deps[i].vp->row, deps[i].vp->col, deps[i].vp->row, deps[i].vp->col, 'd');
                                  }
                                  #endif
                                  if (getVertex(graph, lookat((yyvsp[-1].rval).left.vp->row, (yyvsp[-1].rval).left.vp->col), 0) != NULL) destroy_vertex(lookat((yyvsp[-1].rval).left.vp->row, (yyvsp[-1].rval).left.vp->col));

                                  (yyvsp[-1].rval).left.vp->v = (double) 0.0;
                                  if ((yyvsp[-1].rval).left.vp->expr && !((yyvsp[-1].rval).left.vp->flags & is_strexpr)) {
                                      efree((yyvsp[-1].rval).left.vp->expr);
                                      (yyvsp[-1].rval).left.vp->expr = NULL;
                                  }
                                  (yyvsp[-1].rval).left.vp->cellerror = CELLOK;
                                  (yyvsp[-1].rval).left.vp->flags &= ~is_valid;
                                  (yyvsp[-1].rval).left.vp->flags |= is_changed;
                                  modflg++;

                                  #ifdef UNDO
                                  copy_to_undostruct((yyvsp[-1].rval).left.vp->row, (yyvsp[-1].rval).left.vp->col, (yyvsp[-1].rval).left.vp->row, (yyvsp[-1].rval).left.vp->col, 'a');
                                  // here we save in undostruct, all the ents that depends on the deleted one (after change)
                                  if (deps != NULL) free(deps);
                                  ents_that_depends_on_range((yyvsp[-1].rval).left.vp->row, (yyvsp[-1].rval).left.vp->col, (yyvsp[-1].rval).left.vp->row, (yyvsp[-1].rval).left.vp->col);
                                  if (deps != NULL) {
                                      for (i = 0, n = deps->vf; i < n; i++)
                                          copy_to_undostruct(deps[i].vp->row, deps[i].vp->col, deps[i].vp->row, deps[i].vp->col, 'a');
                                      free(deps);
                                      deps = NULL;
                                  }
                                  end_undo_action();
                                  #endif
                                  }
#line 2698 "y.tab.c" /* yacc.c:1646  */
    break;

  case 5:
#line 447 "gram.y" /* yacc.c:1646  */
    { slet((yyvsp[-2].rval).left.vp, (yyvsp[0].enode), 0); }
#line 2704 "y.tab.c" /* yacc.c:1646  */
    break;

  case 6:
#line 448 "gram.y" /* yacc.c:1646  */
    { slet((yyvsp[-2].rval).left.vp, (yyvsp[0].enode), -1); }
#line 2710 "y.tab.c" /* yacc.c:1646  */
    break;

  case 7:
#line 449 "gram.y" /* yacc.c:1646  */
    { slet((yyvsp[-2].rval).left.vp, (yyvsp[0].enode), 1); }
#line 2716 "y.tab.c" /* yacc.c:1646  */
    break;

  case 8:
#line 450 "gram.y" /* yacc.c:1646  */
    { ljustify((yyvsp[0].rval).left.vp->row, (yyvsp[0].rval).left.vp->col, (yyvsp[0].rval).right.vp->row, (yyvsp[0].rval).right.vp->col); }
#line 2722 "y.tab.c" /* yacc.c:1646  */
    break;

  case 9:
#line 451 "gram.y" /* yacc.c:1646  */
    { rjustify((yyvsp[0].rval).left.vp->row, (yyvsp[0].rval).left.vp->col, (yyvsp[0].rval).right.vp->row, (yyvsp[0].rval).right.vp->col); }
#line 2728 "y.tab.c" /* yacc.c:1646  */
    break;

  case 10:
#line 452 "gram.y" /* yacc.c:1646  */
    { center((yyvsp[0].rval).left.vp->row, (yyvsp[0].rval).left.vp->col, (yyvsp[0].rval).right.vp->row, (yyvsp[0].rval).right.vp->col); }
#line 2734 "y.tab.c" /* yacc.c:1646  */
    break;

  case 11:
#line 453 "gram.y" /* yacc.c:1646  */
    { doformat((yyvsp[-3].ival),(yyvsp[-3].ival),(yyvsp[-2].ival),(yyvsp[-1].ival),(yyvsp[0].ival)); }
#line 2740 "y.tab.c" /* yacc.c:1646  */
    break;

  case 12:
#line 454 "gram.y" /* yacc.c:1646  */
    { format_cell((yyvsp[-1].rval).left.vp, (yyvsp[-1].rval).right.vp, (yyvsp[0].sval));
                                       scxfree((yyvsp[0].sval));
                                     }
#line 2748 "y.tab.c" /* yacc.c:1646  */
    break;

  case 13:
#line 458 "gram.y" /* yacc.c:1646  */
    { dateformat((yyvsp[-1].rval).left.vp, (yyvsp[-1].rval).right.vp, (yyvsp[0].sval));
                                       scxfree((yyvsp[0].sval)); }
#line 2755 "y.tab.c" /* yacc.c:1646  */
    break;

  case 14:
#line 460 "gram.y" /* yacc.c:1646  */
    { dateformat(lookat(currow, curcol), lookat(currow, curcol), (yyvsp[0].sval));
                                       scxfree((yyvsp[0].sval)); }
#line 2762 "y.tab.c" /* yacc.c:1646  */
    break;

  case 15:
#line 463 "gram.y" /* yacc.c:1646  */
    { hide_col((yyvsp[0].ival), 1); }
#line 2768 "y.tab.c" /* yacc.c:1646  */
    break;

  case 16:
#line 464 "gram.y" /* yacc.c:1646  */
    { hide_row((yyvsp[0].ival), 1); }
#line 2774 "y.tab.c" /* yacc.c:1646  */
    break;

  case 17:
#line 465 "gram.y" /* yacc.c:1646  */
    { show_col((yyvsp[0].ival), 1); }
#line 2780 "y.tab.c" /* yacc.c:1646  */
    break;

  case 18:
#line 466 "gram.y" /* yacc.c:1646  */
    { show_row((yyvsp[0].ival), 1); }
#line 2786 "y.tab.c" /* yacc.c:1646  */
    break;

  case 19:
#line 469 "gram.y" /* yacc.c:1646  */
    {
                                       hide_col((yyvsp[0].ival), 1); }
#line 2793 "y.tab.c" /* yacc.c:1646  */
    break;

  case 20:
#line 471 "gram.y" /* yacc.c:1646  */
    {
                                       show_col((yyvsp[0].ival), 1); }
#line 2800 "y.tab.c" /* yacc.c:1646  */
    break;

  case 21:
#line 473 "gram.y" /* yacc.c:1646  */
    {
                                       hide_row((yyvsp[0].ival), 1); }
#line 2807 "y.tab.c" /* yacc.c:1646  */
    break;

  case 22:
#line 475 "gram.y" /* yacc.c:1646  */
    {
                                       show_row((yyvsp[0].ival), 1); }
#line 2814 "y.tab.c" /* yacc.c:1646  */
    break;

  case 23:
#line 477 "gram.y" /* yacc.c:1646  */
    {
                                       show_col((yyvsp[-2].ival), (yyvsp[0].ival)-(yyvsp[-2].ival)+1); }
#line 2821 "y.tab.c" /* yacc.c:1646  */
    break;

  case 24:
#line 479 "gram.y" /* yacc.c:1646  */
    {
                                       show_row((yyvsp[-2].ival), (yyvsp[0].ival)-(yyvsp[-2].ival)+1); }
#line 2828 "y.tab.c" /* yacc.c:1646  */
    break;

  case 25:
#line 481 "gram.y" /* yacc.c:1646  */
    {
                                       int c = curcol, arg;
                                       if ((yyvsp[-2].ival) < (yyvsp[0].ival)) {
                                            curcol = (yyvsp[-2].ival);
                                            arg = (yyvsp[0].ival) - (yyvsp[-2].ival) + 1;
                                       } else {
                                            curcol = (yyvsp[0].ival);
                                            arg = (yyvsp[-2].ival) - (yyvsp[0].ival) + 1;
                                       }
                                       hide_col((yyvsp[-2].ival), arg);        // hide de un rango de columnas
                                       curcol = c < curcol ? c : c < curcol + arg ? curcol : c - arg;
                                     }
#line 2845 "y.tab.c" /* yacc.c:1646  */
    break;

  case 26:
#line 493 "gram.y" /* yacc.c:1646  */
    {
                                       int r = currow, arg;      // hide de un rango de filas
                                       if ((yyvsp[-2].ival) < (yyvsp[0].ival)) {
                                           currow = (yyvsp[-2].ival);
                                           arg = (yyvsp[0].ival) - (yyvsp[-2].ival) + 1;
                                       } else {
                                           currow = (yyvsp[0].ival);
                                           arg = (yyvsp[-2].ival) - (yyvsp[0].ival) + 1;
                                       }
                                       hide_row((yyvsp[-2].ival), arg);
                                       currow = r < currow ? r : r < currow + arg ? currow : r - arg;
                                     }
#line 2862 "y.tab.c" /* yacc.c:1646  */
    break;

  case 27:
#line 506 "gram.y" /* yacc.c:1646  */
    {
                                       if (strlen((yyvsp[0].sval)) != 1 || ((yyvsp[0].sval)[0] != 'h' && (yyvsp[0].sval)[0] != 'j' && (yyvsp[0].sval)[0] != 'k' && (yyvsp[0].sval)[0] != 'l')) {
                                           sc_error("wrong parameter for shift command");
                                       } else {
                                           wchar_t wstr[2] = L"";
                                           swprintf(wstr, BUFFERSIZE, L"%c", (yyvsp[0].sval)[0]);
                                           shift((yyvsp[-1].rval).left.vp->row, (yyvsp[-1].rval).left.vp->col, (yyvsp[-1].rval).right.vp->row, (yyvsp[-1].rval).right.vp->col, wstr[0]);
                                       }
                                       scxfree((yyvsp[0].sval));
                                     }
#line 2877 "y.tab.c" /* yacc.c:1646  */
    break;

  case 28:
#line 517 "gram.y" /* yacc.c:1646  */
    { set_cell_mark((yyvsp[-1].ival) + 97, (yyvsp[0].rval).left.vp->row, (yyvsp[0].rval).left.vp->col); }
#line 2883 "y.tab.c" /* yacc.c:1646  */
    break;

  case 29:
#line 519 "gram.y" /* yacc.c:1646  */
    { ;
                                          srange * sr = create_range('\0', '\0', (yyvsp[-1].rval).left.vp, (yyvsp[0].rval).left.vp);
                                          unselect_ranges();
                                          set_range_mark((yyvsp[-2].ival) + 97, sr);
                                          }
#line 2893 "y.tab.c" /* yacc.c:1646  */
    break;

  case 30:
#line 525 "gram.y" /* yacc.c:1646  */
    { fill((yyvsp[-2].rval).left.vp, (yyvsp[-2].rval).right.vp, (yyvsp[-1].fval), (yyvsp[0].fval)); }
#line 2899 "y.tab.c" /* yacc.c:1646  */
    break;

  case 31:
#line 527 "gram.y" /* yacc.c:1646  */
    { sc_error("Not enough parameters for fill command"); }
#line 2905 "y.tab.c" /* yacc.c:1646  */
    break;

  case 32:
#line 529 "gram.y" /* yacc.c:1646  */
    { remove_frange(); }
#line 2911 "y.tab.c" /* yacc.c:1646  */
    break;

  case 33:
#line 530 "gram.y" /* yacc.c:1646  */
    { add_frange((yyvsp[0].rval).left.vp, (yyvsp[0].rval).right.vp, 'a'); }
#line 2917 "y.tab.c" /* yacc.c:1646  */
    break;

  case 34:
#line 531 "gram.y" /* yacc.c:1646  */
    { add_frange(lookat((yyvsp[-2].ival), 0), lookat((yyvsp[0].ival), 0), 'r'); }
#line 2923 "y.tab.c" /* yacc.c:1646  */
    break;

  case 35:
#line 532 "gram.y" /* yacc.c:1646  */
    { add_frange(lookat((yyvsp[0].ival), 0), lookat((yyvsp[0].ival), 0), 'r'); }
#line 2929 "y.tab.c" /* yacc.c:1646  */
    break;

  case 36:
#line 533 "gram.y" /* yacc.c:1646  */
    { add_frange(lookat(0, (yyvsp[-2].ival)), lookat(0, (yyvsp[0].ival)), 'c'); }
#line 2935 "y.tab.c" /* yacc.c:1646  */
    break;

  case 37:
#line 534 "gram.y" /* yacc.c:1646  */
    { add_frange(lookat(0, (yyvsp[0].ival)), lookat(0, (yyvsp[0].ival)), 'c'); }
#line 2941 "y.tab.c" /* yacc.c:1646  */
    break;

  case 38:
#line 536 "gram.y" /* yacc.c:1646  */
    { sortrange((yyvsp[-1].rval).left.vp, (yyvsp[-1].rval).right.vp, (yyvsp[0].sval));
                                       //scxfree($3);
                                       //do not free here
                                     }
#line 2950 "y.tab.c" /* yacc.c:1646  */
    break;

  case 39:
#line 540 "gram.y" /* yacc.c:1646  */
    {
                                       subtotal((yyvsp[-3].rval).left.vp->row, (yyvsp[-3].rval).left.vp->col, (yyvsp[-3].rval).right.vp->row,
                                                (yyvsp[-3].rval).right.vp->col, (yyvsp[-2].ival), (yyvsp[-1].sval), (yyvsp[0].ival), 0);
                                       scxfree((yyvsp[-1].sval));
                                     }
#line 2960 "y.tab.c" /* yacc.c:1646  */
    break;

  case 40:
#line 545 "gram.y" /* yacc.c:1646  */
    {
                                       subtotal((yyvsp[-3].rval).left.vp->row, (yyvsp[-3].rval).left.vp->col, (yyvsp[-3].rval).right.vp->row,
                                                 (yyvsp[-3].rval).right.vp->col, (yyvsp[-2].ival), (yyvsp[-1].sval), (yyvsp[0].ival), 1);
                                       scxfree((yyvsp[-1].sval));
                                     }
#line 2970 "y.tab.c" /* yacc.c:1646  */
    break;

  case 41:
#line 550 "gram.y" /* yacc.c:1646  */
    { enable_filters((yyvsp[0].rval).left.vp, (yyvsp[0].rval).right.vp);
                                     }
#line 2977 "y.tab.c" /* yacc.c:1646  */
    break;

  case 42:
#line 561 "gram.y" /* yacc.c:1646  */
    { auto_justify((yyvsp[-2].ival), (yyvsp[0].ival), DEFWIDTH); }
#line 2983 "y.tab.c" /* yacc.c:1646  */
    break;

  case 43:
#line 562 "gram.y" /* yacc.c:1646  */
    { auto_justify((yyvsp[0].ival), (yyvsp[0].ival), DEFWIDTH); }
#line 2989 "y.tab.c" /* yacc.c:1646  */
    break;

  case 44:
#line 564 "gram.y" /* yacc.c:1646  */
    { moveto((yyvsp[-1].rval).left.vp->row, (yyvsp[-1].rval).left.vp->col, (yyvsp[-1].rval).right.vp->row, (yyvsp[-1].rval).right.vp->col, (yyvsp[0].rval).left.vp->row, (yyvsp[0].rval).left.vp->col); }
#line 2995 "y.tab.c" /* yacc.c:1646  */
    break;

  case 45:
#line 565 "gram.y" /* yacc.c:1646  */
    { moveto((yyvsp[0].rval).left.vp->row, (yyvsp[0].rval).left.vp->col, (yyvsp[0].rval).right.vp->row, (yyvsp[0].rval).right.vp->col, -1, -1); }
#line 3001 "y.tab.c" /* yacc.c:1646  */
    break;

  case 46:
#line 566 "gram.y" /* yacc.c:1646  */
    { num_search((yyvsp[0].fval), 0, 0, maxrow, maxcol, 0, 1); }
#line 3007 "y.tab.c" /* yacc.c:1646  */
    break;

  case 47:
#line 567 "gram.y" /* yacc.c:1646  */
    { str_search((yyvsp[0].sval), 0, 0, maxrow, maxcol, 0, 1); }
#line 3013 "y.tab.c" /* yacc.c:1646  */
    break;

  case 48:
#line 569 "gram.y" /* yacc.c:1646  */
    { str_search((yyvsp[0].sval), 0, 0, maxrow, maxcol, 1, 1); }
#line 3019 "y.tab.c" /* yacc.c:1646  */
    break;

  case 49:
#line 571 "gram.y" /* yacc.c:1646  */
    { str_search((yyvsp[0].sval), 0, 0, maxrow, maxcol, 2, 1); }
#line 3025 "y.tab.c" /* yacc.c:1646  */
    break;

  case 50:
#line 575 "gram.y" /* yacc.c:1646  */
    { copy_to_clipboard((yyvsp[0].rval).left.vp->row, (yyvsp[0].rval).left.vp->col, (yyvsp[0].rval).right.vp->row, (yyvsp[0].rval).right.vp->col); }
#line 3031 "y.tab.c" /* yacc.c:1646  */
    break;

  case 51:
#line 576 "gram.y" /* yacc.c:1646  */
    { paste_from_clipboard(); }
#line 3037 "y.tab.c" /* yacc.c:1646  */
    break;

  case 52:
#line 577 "gram.y" /* yacc.c:1646  */
    { lock_cells((yyvsp[0].rval).left.vp, (yyvsp[0].rval).right.vp); }
#line 3043 "y.tab.c" /* yacc.c:1646  */
    break;

  case 53:
#line 578 "gram.y" /* yacc.c:1646  */
    { unlock_cells((yyvsp[0].rval).left.vp, (yyvsp[0].rval).right.vp); }
#line 3049 "y.tab.c" /* yacc.c:1646  */
    break;

  case 54:
#line 579 "gram.y" /* yacc.c:1646  */
    {
                                   add_map((yyvsp[-1].sval), (yyvsp[0].sval), NORMAL_MODE, 1);
                                   scxfree((yyvsp[-1].sval));
                                   scxfree((yyvsp[0].sval));
                                 }
#line 3059 "y.tab.c" /* yacc.c:1646  */
    break;

  case 55:
#line 585 "gram.y" /* yacc.c:1646  */
    {
                                   add_map((yyvsp[-1].sval), (yyvsp[0].sval), INSERT_MODE, 1);
                                   scxfree((yyvsp[-1].sval));
                                   scxfree((yyvsp[0].sval));
                                 }
#line 3069 "y.tab.c" /* yacc.c:1646  */
    break;

  case 56:
#line 590 "gram.y" /* yacc.c:1646  */
    {
                                   add_map((yyvsp[-1].sval), (yyvsp[0].sval), NORMAL_MODE, 0);
                                   scxfree((yyvsp[-1].sval));
                                   scxfree((yyvsp[0].sval));
                                   }
#line 3079 "y.tab.c" /* yacc.c:1646  */
    break;

  case 57:
#line 596 "gram.y" /* yacc.c:1646  */
    {
                                   add_map((yyvsp[-1].sval), (yyvsp[0].sval), INSERT_MODE, 0);
                                   scxfree((yyvsp[-1].sval));
                                   scxfree((yyvsp[0].sval));
                                   }
#line 3089 "y.tab.c" /* yacc.c:1646  */
    break;

  case 58:
#line 602 "gram.y" /* yacc.c:1646  */
    {
                                   del_map((yyvsp[0].sval), NORMAL_MODE);
                                   scxfree((yyvsp[0].sval));
                                   }
#line 3098 "y.tab.c" /* yacc.c:1646  */
    break;

  case 59:
#line 607 "gram.y" /* yacc.c:1646  */
    {
                                   del_map((yyvsp[0].sval), INSERT_MODE);
                                   scxfree((yyvsp[0].sval));
                                   }
#line 3107 "y.tab.c" /* yacc.c:1646  */
    break;

  case 60:
#line 612 "gram.y" /* yacc.c:1646  */
    {
#ifdef USECOLORS
                                   chg_color((yyvsp[0].sval));
#endif
                                   scxfree((yyvsp[0].sval));
                                   }
#line 3118 "y.tab.c" /* yacc.c:1646  */
    break;

  case 61:
#line 620 "gram.y" /* yacc.c:1646  */
    {
#ifdef USECOLORS
                                   if ( ! atoi(get_conf_value("nocurses")))
                                       color_cell((yyvsp[-1].rval).left.vp->row, (yyvsp[-1].rval).left.vp->col, (yyvsp[-1].rval).right.vp->row, (yyvsp[-1].rval).right.vp->col, (yyvsp[0].sval));
#endif
                                   scxfree((yyvsp[0].sval));
                                   }
#line 3130 "y.tab.c" /* yacc.c:1646  */
    break;

  case 62:
#line 628 "gram.y" /* yacc.c:1646  */
    {
                                          set_trigger((yyvsp[-1].rval).left.vp->row, (yyvsp[-1].rval).left.vp->col, (yyvsp[-1].rval).right.vp->row, (yyvsp[-1].rval).right.vp->col, (yyvsp[0].sval));
                                          scxfree((yyvsp[0].sval));
                                        }
#line 3139 "y.tab.c" /* yacc.c:1646  */
    break;

  case 63:
#line 633 "gram.y" /* yacc.c:1646  */
    {
                                   del_trigger((yyvsp[0].rval).left.vp->row, (yyvsp[0].rval).left.vp->col, (yyvsp[0].rval).right.vp->row, (yyvsp[0].rval).right.vp->col);
                                   }
#line 3147 "y.tab.c" /* yacc.c:1646  */
    break;

  case 64:
#line 637 "gram.y" /* yacc.c:1646  */
    {
#ifdef USECOLORS
                                   if ( ! atoi(get_conf_value("nocurses")))
                                       color_cell(currow, curcol, currow, curcol, (yyvsp[0].sval));
#endif
                                   scxfree((yyvsp[0].sval));
                                   }
#line 3159 "y.tab.c" /* yacc.c:1646  */
    break;

  case 65:
#line 645 "gram.y" /* yacc.c:1646  */
    {
#ifdef USECOLORS
                                   if ( ! atoi(get_conf_value("nocurses"))) unformat((yyvsp[0].rval).left.vp->row, (yyvsp[0].rval).left.vp->col, (yyvsp[0].rval).right.vp->row, (yyvsp[0].rval).right.vp->col);
#endif
                                   }
#line 3169 "y.tab.c" /* yacc.c:1646  */
    break;

  case 66:
#line 651 "gram.y" /* yacc.c:1646  */
    {
#ifdef USECOLORS
                                   if ( ! atoi(get_conf_value("nocurses"))) unformat(currow, curcol, currow, curcol);
#endif
                                   }
#line 3179 "y.tab.c" /* yacc.c:1646  */
    break;

  case 67:
#line 657 "gram.y" /* yacc.c:1646  */
    {
                                         redefine_color((yyvsp[-3].sval), (yyvsp[-2].ival), (yyvsp[-1].ival), (yyvsp[0].ival));
                                         scxfree((yyvsp[-3].sval)); }
#line 3187 "y.tab.c" /* yacc.c:1646  */
    break;

  case 68:
#line 661 "gram.y" /* yacc.c:1646  */
    { fcopy(); }
#line 3193 "y.tab.c" /* yacc.c:1646  */
    break;

  case 69:
#line 662 "gram.y" /* yacc.c:1646  */
    { fsum();  }
#line 3199 "y.tab.c" /* yacc.c:1646  */
    break;

  case 70:
#line 663 "gram.y" /* yacc.c:1646  */
    { pad((yyvsp[-3].ival), 0, (yyvsp[-2].ival), maxrow, (yyvsp[0].ival)); }
#line 3205 "y.tab.c" /* yacc.c:1646  */
    break;

  case 71:
#line 664 "gram.y" /* yacc.c:1646  */
    { pad((yyvsp[-1].ival), 0, (yyvsp[0].ival), maxrow, (yyvsp[0].ival)); }
#line 3211 "y.tab.c" /* yacc.c:1646  */
    break;

  case 72:
#line 665 "gram.y" /* yacc.c:1646  */
    { pad((yyvsp[-1].ival), (yyvsp[0].rval).left.vp->row, (yyvsp[0].rval).left.vp->col, (yyvsp[0].rval).right.vp->row, (yyvsp[0].rval).right.vp->col); }
#line 3217 "y.tab.c" /* yacc.c:1646  */
    break;

  case 73:
#line 667 "gram.y" /* yacc.c:1646  */
    {
                                     plot((yyvsp[-1].sval), (yyvsp[0].rval).left.vp->row, (yyvsp[0].rval).left.vp->col, (yyvsp[0].rval).right.vp->row, (yyvsp[0].rval).right.vp->col);
                                     scxfree((yyvsp[-1].sval));
                                   }
#line 3226 "y.tab.c" /* yacc.c:1646  */
    break;

  case 74:
#line 675 "gram.y" /* yacc.c:1646  */
    { }
#line 3232 "y.tab.c" /* yacc.c:1646  */
    break;

  case 75:
#line 688 "gram.y" /* yacc.c:1646  */
    { add_range((yyvsp[-1].sval), (yyvsp[0].rval).left, (yyvsp[0].rval).right, 1); }
#line 3238 "y.tab.c" /* yacc.c:1646  */
    break;

  case 76:
#line 689 "gram.y" /* yacc.c:1646  */
    { add_range((yyvsp[-1].sval), (yyvsp[0].ent), (yyvsp[0].ent), 0); }
#line 3244 "y.tab.c" /* yacc.c:1646  */
    break;

  case 77:
#line 691 "gram.y" /* yacc.c:1646  */
    { del_range((yyvsp[0].rval).left.vp, (yyvsp[0].rval).right.vp); }
#line 3250 "y.tab.c" /* yacc.c:1646  */
    break;

  case 78:
#line 693 "gram.y" /* yacc.c:1646  */
    {
                                     eval_result = eval(NULL, (yyvsp[0].enode));
                                     efree((yyvsp[0].enode));
                                   }
#line 3259 "y.tab.c" /* yacc.c:1646  */
    break;

  case 79:
#line 697 "gram.y" /* yacc.c:1646  */
    {
                                     printf("quitting. unsaved changes will be lost.\n");
                                     shall_quit = 2; // unsaved changes are lost!
                                   }
#line 3268 "y.tab.c" /* yacc.c:1646  */
    break;

  case 80:
#line 701 "gram.y" /* yacc.c:1646  */
    {
                                     rebuild_graph();
                                     ui_update(FALSE);
                                   }
#line 3277 "y.tab.c" /* yacc.c:1646  */
    break;

  case 81:
#line 706 "gram.y" /* yacc.c:1646  */
    { print_vertexs(); }
#line 3283 "y.tab.c" /* yacc.c:1646  */
    break;

  case 82:
#line 707 "gram.y" /* yacc.c:1646  */
    { sync_refs(); }
#line 3289 "y.tab.c" /* yacc.c:1646  */
    break;

  case 83:
#line 708 "gram.y" /* yacc.c:1646  */
    {
                                     do_undo();
                                     // sync_refs();
                                     EvalAll();
                                     ui_update(TRUE);
                                   }
#line 3300 "y.tab.c" /* yacc.c:1646  */
    break;

  case 84:
#line 715 "gram.y" /* yacc.c:1646  */
    {
                                     do_redo();
                                     // sync_refs();
                                     EvalAll();
                                     ui_update(TRUE);
                                   }
#line 3311 "y.tab.c" /* yacc.c:1646  */
    break;

  case 85:
#line 724 "gram.y" /* yacc.c:1646  */
    {
                                     EvalAll();
                                     //ui_update(1);
                                     //changed = 0;
                                   }
#line 3321 "y.tab.c" /* yacc.c:1646  */
    break;

  case 86:
#line 729 "gram.y" /* yacc.c:1646  */
    {
                                     getnum((yyvsp[0].rval).left.vp->row, (yyvsp[0].rval).left.vp->col, (yyvsp[0].rval).right.vp->row, (yyvsp[0].rval).right.vp->col, fdoutput);
                                   }
#line 3329 "y.tab.c" /* yacc.c:1646  */
    break;

  case 87:
#line 733 "gram.y" /* yacc.c:1646  */
    { getstring((yyvsp[0].rval).left.vp->row, (yyvsp[0].rval).left.vp->col, (yyvsp[0].rval).right.vp->row, (yyvsp[0].rval).right.vp->col, fdoutput); }
#line 3335 "y.tab.c" /* yacc.c:1646  */
    break;

  case 88:
#line 735 "gram.y" /* yacc.c:1646  */
    { getexp((yyvsp[0].rval).left.vp->row, (yyvsp[0].rval).left.vp->col, (yyvsp[0].rval).right.vp->row, (yyvsp[0].rval).right.vp->col, fdoutput); }
#line 3341 "y.tab.c" /* yacc.c:1646  */
    break;

  case 89:
#line 737 "gram.y" /* yacc.c:1646  */
    { getformat((yyvsp[0].ival), fdoutput); }
#line 3347 "y.tab.c" /* yacc.c:1646  */
    break;

  case 90:
#line 739 "gram.y" /* yacc.c:1646  */
    { getfmt((yyvsp[0].rval).left.vp->row, (yyvsp[0].rval).left.vp->col, (yyvsp[0].rval).right.vp->row, (yyvsp[0].rval).right.vp->col, fdoutput); }
#line 3353 "y.tab.c" /* yacc.c:1646  */
    break;

  case 91:
#line 741 "gram.y" /* yacc.c:1646  */
    { seval_result = seval(NULL, (yyvsp[0].enode)); // TODO make sure this seval_result is always freed afterwards
                                     efree((yyvsp[0].enode));
                                   }
#line 3361 "y.tab.c" /* yacc.c:1646  */
    break;

  case 92:
#line 744 "gram.y" /* yacc.c:1646  */
    { sc_error((yyvsp[0].sval));
                                     //free $2
                                   }
#line 3369 "y.tab.c" /* yacc.c:1646  */
    break;

  case 94:
#line 749 "gram.y" /* yacc.c:1646  */
    {
                     sc_error("syntax error");
                     line[0]='\0';
                     linelim = 0;
                     yyparse();
                     linelim = -1;
                     yyerrok;
                 }
#line 3382 "y.tab.c" /* yacc.c:1646  */
    break;

  case 95:
#line 758 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new_var(O_VAR, (yyvsp[0].ent)); }
#line 3388 "y.tab.c" /* yacc.c:1646  */
    break;

  case 96:
#line 759 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new('f', (yyvsp[0].enode), ENULL); }
#line 3394 "y.tab.c" /* yacc.c:1646  */
    break;

  case 97:
#line 761 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new('F', (yyvsp[0].enode), ENULL); }
#line 3400 "y.tab.c" /* yacc.c:1646  */
    break;

  case 98:
#line 763 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(SUM, new_range(REDUCE | SUM, (yyvsp[-1].rval)), ENULL); }
#line 3406 "y.tab.c" /* yacc.c:1646  */
    break;

  case 99:
#line 765 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(SUM, new_range(REDUCE | SUM, (yyvsp[-3].rval)), (yyvsp[-1].enode)); }
#line 3412 "y.tab.c" /* yacc.c:1646  */
    break;

  case 100:
#line 767 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(PROD, new_range(REDUCE | PROD, (yyvsp[-1].rval)), ENULL); }
#line 3418 "y.tab.c" /* yacc.c:1646  */
    break;

  case 101:
#line 769 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(PROD, new_range(REDUCE | PROD, (yyvsp[-3].rval)), (yyvsp[-1].enode)); }
#line 3424 "y.tab.c" /* yacc.c:1646  */
    break;

  case 102:
#line 771 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(AVG, new_range(REDUCE | AVG, (yyvsp[-1].rval)), ENULL); }
#line 3430 "y.tab.c" /* yacc.c:1646  */
    break;

  case 103:
#line 773 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(AVG, new_range(REDUCE | AVG, (yyvsp[-3].rval)), (yyvsp[-1].enode)); }
#line 3436 "y.tab.c" /* yacc.c:1646  */
    break;

  case 104:
#line 775 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(STDDEV, new_range(REDUCE | STDDEV, (yyvsp[-1].rval)), ENULL); }
#line 3442 "y.tab.c" /* yacc.c:1646  */
    break;

  case 105:
#line 777 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(STDDEV, new_range(REDUCE | STDDEV, (yyvsp[-3].rval)), (yyvsp[-1].enode)); }
#line 3448 "y.tab.c" /* yacc.c:1646  */
    break;

  case 106:
#line 779 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(COUNT, new_range(REDUCE | COUNT, (yyvsp[-1].rval)), ENULL); }
#line 3454 "y.tab.c" /* yacc.c:1646  */
    break;

  case 107:
#line 781 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(COUNT, new_range(REDUCE | COUNT, (yyvsp[-3].rval)), (yyvsp[-1].enode)); }
#line 3460 "y.tab.c" /* yacc.c:1646  */
    break;

  case 108:
#line 783 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(MAX, new_range(REDUCE | MAX, (yyvsp[-1].rval)), ENULL); }
#line 3466 "y.tab.c" /* yacc.c:1646  */
    break;

  case 109:
#line 785 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(MAX, new_range(REDUCE | MAX, (yyvsp[-3].rval)), (yyvsp[-1].enode)); }
#line 3472 "y.tab.c" /* yacc.c:1646  */
    break;

  case 110:
#line 787 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(LMAX, (yyvsp[-1].enode), (yyvsp[-3].enode)); }
#line 3478 "y.tab.c" /* yacc.c:1646  */
    break;

  case 111:
#line 789 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(MIN, new_range(REDUCE | MIN, (yyvsp[-1].rval)), ENULL); }
#line 3484 "y.tab.c" /* yacc.c:1646  */
    break;

  case 112:
#line 791 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(MIN, new_range(REDUCE | MIN, (yyvsp[-3].rval)), (yyvsp[-1].enode)); }
#line 3490 "y.tab.c" /* yacc.c:1646  */
    break;

  case 113:
#line 793 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(LMIN, (yyvsp[-1].enode), (yyvsp[-3].enode)); }
#line 3496 "y.tab.c" /* yacc.c:1646  */
    break;

  case 114:
#line 795 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new_range(REDUCE | 'R', (yyvsp[-1].rval)); }
#line 3502 "y.tab.c" /* yacc.c:1646  */
    break;

  case 115:
#line 797 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new_range(REDUCE | 'C', (yyvsp[-1].rval)); }
#line 3508 "y.tab.c" /* yacc.c:1646  */
    break;

  case 116:
#line 798 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(ABS, (yyvsp[-1].enode), ENULL); }
#line 3514 "y.tab.c" /* yacc.c:1646  */
    break;

  case 117:
#line 799 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(FROW, (yyvsp[-1].enode), ENULL); }
#line 3520 "y.tab.c" /* yacc.c:1646  */
    break;

  case 118:
#line 800 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(FCOL, (yyvsp[-1].enode), ENULL); }
#line 3526 "y.tab.c" /* yacc.c:1646  */
    break;

  case 119:
#line 801 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(ACOS, (yyvsp[-1].enode), ENULL); }
#line 3532 "y.tab.c" /* yacc.c:1646  */
    break;

  case 120:
#line 802 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(ASIN, (yyvsp[-1].enode), ENULL); }
#line 3538 "y.tab.c" /* yacc.c:1646  */
    break;

  case 121:
#line 803 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(ATAN, (yyvsp[-1].enode), ENULL); }
#line 3544 "y.tab.c" /* yacc.c:1646  */
    break;

  case 122:
#line 805 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(ATAN2, (yyvsp[-3].enode), (yyvsp[-1].enode)); }
#line 3550 "y.tab.c" /* yacc.c:1646  */
    break;

  case 123:
#line 806 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(CEIL, (yyvsp[-1].enode), ENULL); }
#line 3556 "y.tab.c" /* yacc.c:1646  */
    break;

  case 124:
#line 807 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(COS, (yyvsp[-1].enode), ENULL); }
#line 3562 "y.tab.c" /* yacc.c:1646  */
    break;

  case 125:
#line 808 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(EXP, (yyvsp[-1].enode), ENULL); }
#line 3568 "y.tab.c" /* yacc.c:1646  */
    break;

  case 126:
#line 809 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(FABS, (yyvsp[-1].enode), ENULL); }
#line 3574 "y.tab.c" /* yacc.c:1646  */
    break;

  case 127:
#line 810 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(FLOOR, (yyvsp[-1].enode), ENULL); }
#line 3580 "y.tab.c" /* yacc.c:1646  */
    break;

  case 128:
#line 812 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(HYPOT, (yyvsp[-3].enode), (yyvsp[-1].enode)); }
#line 3586 "y.tab.c" /* yacc.c:1646  */
    break;

  case 129:
#line 813 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(LOG, (yyvsp[-1].enode), ENULL); }
#line 3592 "y.tab.c" /* yacc.c:1646  */
    break;

  case 130:
#line 814 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(LOG10, (yyvsp[-1].enode), ENULL); }
#line 3598 "y.tab.c" /* yacc.c:1646  */
    break;

  case 131:
#line 816 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(POW, (yyvsp[-3].enode), (yyvsp[-1].enode)); }
#line 3604 "y.tab.c" /* yacc.c:1646  */
    break;

  case 132:
#line 817 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(SIN, (yyvsp[-1].enode), ENULL); }
#line 3610 "y.tab.c" /* yacc.c:1646  */
    break;

  case 133:
#line 818 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(SQRT, (yyvsp[-1].enode), ENULL); }
#line 3616 "y.tab.c" /* yacc.c:1646  */
    break;

  case 134:
#line 819 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(TAN, (yyvsp[-1].enode), ENULL); }
#line 3622 "y.tab.c" /* yacc.c:1646  */
    break;

  case 135:
#line 820 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(DTR, (yyvsp[-1].enode), ENULL); }
#line 3628 "y.tab.c" /* yacc.c:1646  */
    break;

  case 136:
#line 821 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(RTD, (yyvsp[-1].enode), ENULL); }
#line 3634 "y.tab.c" /* yacc.c:1646  */
    break;

  case 137:
#line 822 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(RND, (yyvsp[-1].enode), ENULL); }
#line 3640 "y.tab.c" /* yacc.c:1646  */
    break;

  case 138:
#line 824 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(ROUND, (yyvsp[-3].enode), (yyvsp[-1].enode)); }
#line 3646 "y.tab.c" /* yacc.c:1646  */
    break;

  case 139:
#line 826 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(IF,  (yyvsp[-5].enode),new(',',(yyvsp[-3].enode),(yyvsp[-1].enode))); }
#line 3652 "y.tab.c" /* yacc.c:1646  */
    break;

  case 140:
#line 828 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(PV,  (yyvsp[-5].enode),new(':',(yyvsp[-3].enode),(yyvsp[-1].enode))); }
#line 3658 "y.tab.c" /* yacc.c:1646  */
    break;

  case 141:
#line 830 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(FV,  (yyvsp[-5].enode),new(':',(yyvsp[-3].enode),(yyvsp[-1].enode))); }
#line 3664 "y.tab.c" /* yacc.c:1646  */
    break;

  case 142:
#line 832 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(PMT, (yyvsp[-5].enode),new(':',(yyvsp[-3].enode),(yyvsp[-1].enode))); }
#line 3670 "y.tab.c" /* yacc.c:1646  */
    break;

  case 143:
#line 833 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(HOUR, (yyvsp[-1].enode), ENULL); }
#line 3676 "y.tab.c" /* yacc.c:1646  */
    break;

  case 144:
#line 834 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(MINUTE, (yyvsp[-1].enode), ENULL); }
#line 3682 "y.tab.c" /* yacc.c:1646  */
    break;

  case 145:
#line 835 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(SECOND, (yyvsp[-1].enode), ENULL); }
#line 3688 "y.tab.c" /* yacc.c:1646  */
    break;

  case 146:
#line 836 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(MONTH, (yyvsp[-1].enode), ENULL); }
#line 3694 "y.tab.c" /* yacc.c:1646  */
    break;

  case 147:
#line 837 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(DAY, (yyvsp[-1].enode), ENULL); }
#line 3700 "y.tab.c" /* yacc.c:1646  */
    break;

  case 148:
#line 838 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(YEAR, (yyvsp[-1].enode), ENULL); }
#line 3706 "y.tab.c" /* yacc.c:1646  */
    break;

  case 149:
#line 839 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(NOW, ENULL, ENULL);}
#line 3712 "y.tab.c" /* yacc.c:1646  */
    break;

  case 150:
#line 841 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(DTS, (yyvsp[-5].enode), new(',', (yyvsp[-3].enode), (yyvsp[-1].enode)));}
#line 3718 "y.tab.c" /* yacc.c:1646  */
    break;

  case 151:
#line 843 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(DTS, new_const(O_CONST, (double) (yyvsp[-4].ival)),
                                         new(',', new_const(O_CONST, (double) (yyvsp[-2].ival)),
                                         new_const(O_CONST, (double) (yyvsp[0].ival))));}
#line 3726 "y.tab.c" /* yacc.c:1646  */
    break;

  case 152:
#line 847 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(TTS, (yyvsp[-5].enode), new(',', (yyvsp[-3].enode), (yyvsp[-1].enode)));}
#line 3732 "y.tab.c" /* yacc.c:1646  */
    break;

  case 153:
#line 848 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(STON, (yyvsp[-1].enode), ENULL); }
#line 3738 "y.tab.c" /* yacc.c:1646  */
    break;

  case 154:
#line 849 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(SLEN, (yyvsp[-1].enode), ENULL); }
#line 3744 "y.tab.c" /* yacc.c:1646  */
    break;

  case 155:
#line 851 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(EQS, (yyvsp[-3].enode), (yyvsp[-1].enode)); }
#line 3750 "y.tab.c" /* yacc.c:1646  */
    break;

  case 156:
#line 852 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(DATE, (yyvsp[-1].enode), ENULL); }
#line 3756 "y.tab.c" /* yacc.c:1646  */
    break;

  case 157:
#line 854 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(DATE, (yyvsp[-3].enode), (yyvsp[-1].enode)); }
#line 3762 "y.tab.c" /* yacc.c:1646  */
    break;

  case 158:
#line 856 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(FMT, (yyvsp[-3].enode), (yyvsp[-1].enode)); }
#line 3768 "y.tab.c" /* yacc.c:1646  */
    break;

  case 159:
#line 857 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(UPPER, (yyvsp[-1].enode), ENULL); }
#line 3774 "y.tab.c" /* yacc.c:1646  */
    break;

  case 160:
#line 858 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(LOWER, (yyvsp[-1].enode), ENULL); }
#line 3780 "y.tab.c" /* yacc.c:1646  */
    break;

  case 161:
#line 859 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(CAPITAL, (yyvsp[-1].enode), ENULL); }
#line 3786 "y.tab.c" /* yacc.c:1646  */
    break;

  case 162:
#line 861 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(INDEX, new_range(REDUCE | INDEX, (yyvsp[-3].rval)), (yyvsp[-1].enode)); }
#line 3792 "y.tab.c" /* yacc.c:1646  */
    break;

  case 163:
#line 863 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(INDEX, new_range(REDUCE | INDEX, (yyvsp[-1].rval)), (yyvsp[-3].enode)); }
#line 3798 "y.tab.c" /* yacc.c:1646  */
    break;

  case 164:
#line 865 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(INDEX, new_range(REDUCE | INDEX, (yyvsp[-5].rval)), new(',', (yyvsp[-3].enode), (yyvsp[-1].enode))); }
#line 3804 "y.tab.c" /* yacc.c:1646  */
    break;

  case 165:
#line 867 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(LOOKUP, new_range(REDUCE | LOOKUP, (yyvsp[-3].rval)), (yyvsp[-1].enode)); }
#line 3810 "y.tab.c" /* yacc.c:1646  */
    break;

  case 166:
#line 869 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(LOOKUP, new_range(REDUCE | LOOKUP, (yyvsp[-1].rval)), (yyvsp[-3].enode)); }
#line 3816 "y.tab.c" /* yacc.c:1646  */
    break;

  case 167:
#line 871 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(HLOOKUP, new_range(REDUCE | HLOOKUP, (yyvsp[-5].rval)), new(',', (yyvsp[-3].enode), (yyvsp[-1].enode))); }
#line 3822 "y.tab.c" /* yacc.c:1646  */
    break;

  case 168:
#line 873 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(HLOOKUP, new_range(REDUCE | HLOOKUP, (yyvsp[-3].rval)), new(',', (yyvsp[-5].enode), (yyvsp[-1].enode))); }
#line 3828 "y.tab.c" /* yacc.c:1646  */
    break;

  case 169:
#line 875 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(VLOOKUP, new_range(REDUCE | VLOOKUP, (yyvsp[-5].rval)), new(',', (yyvsp[-3].enode), (yyvsp[-1].enode))); }
#line 3834 "y.tab.c" /* yacc.c:1646  */
    break;

  case 170:
#line 877 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(VLOOKUP, new_range(REDUCE | VLOOKUP, (yyvsp[-3].rval)), new(',', (yyvsp[-5].enode), (yyvsp[-1].enode))); }
#line 3840 "y.tab.c" /* yacc.c:1646  */
    break;

  case 171:
#line 879 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(STINDEX, new_range(REDUCE | STINDEX, (yyvsp[-3].rval)), (yyvsp[-1].enode)); }
#line 3846 "y.tab.c" /* yacc.c:1646  */
    break;

  case 172:
#line 881 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(STINDEX, new_range(REDUCE | STINDEX, (yyvsp[-1].rval)), (yyvsp[-3].enode)); }
#line 3852 "y.tab.c" /* yacc.c:1646  */
    break;

  case 173:
#line 883 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(STINDEX, new_range(REDUCE | STINDEX, (yyvsp[-5].rval)), new(',', (yyvsp[-3].enode), (yyvsp[-1].enode))); }
#line 3858 "y.tab.c" /* yacc.c:1646  */
    break;

  case 174:
#line 885 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(EXT, (yyvsp[-3].enode), (yyvsp[-1].enode)); }
#line 3864 "y.tab.c" /* yacc.c:1646  */
    break;

  case 175:
#line 887 "gram.y" /* yacc.c:1646  */
    {
                                  #ifdef XLUA
                                  (yyval.enode) = new(LUA, (yyvsp[-3].enode), (yyvsp[-1].enode));
                                  #endif
                                  }
#line 3874 "y.tab.c" /* yacc.c:1646  */
    break;

  case 176:
#line 893 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(NVAL, (yyvsp[-3].enode), (yyvsp[-1].enode)); }
#line 3880 "y.tab.c" /* yacc.c:1646  */
    break;

  case 177:
#line 895 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(SVAL, (yyvsp[-3].enode), (yyvsp[-1].enode)); }
#line 3886 "y.tab.c" /* yacc.c:1646  */
    break;

  case 178:
#line 897 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(REPLACE, (yyvsp[-5].enode), new(',', (yyvsp[-3].enode), (yyvsp[-1].enode))); }
#line 3892 "y.tab.c" /* yacc.c:1646  */
    break;

  case 179:
#line 899 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(SUBSTR, (yyvsp[-5].enode), new(',', (yyvsp[-3].enode), (yyvsp[-1].enode))); }
#line 3898 "y.tab.c" /* yacc.c:1646  */
    break;

  case 180:
#line 900 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = (yyvsp[-1].enode); }
#line 3904 "y.tab.c" /* yacc.c:1646  */
    break;

  case 181:
#line 901 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = (yyvsp[0].enode); }
#line 3910 "y.tab.c" /* yacc.c:1646  */
    break;

  case 182:
#line 903 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new_const(O_CONST, (double) (yyvsp[0].ival)); }
#line 3916 "y.tab.c" /* yacc.c:1646  */
    break;

  case 183:
#line 904 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new_const(O_CONST, (yyvsp[0].fval)); }
#line 3922 "y.tab.c" /* yacc.c:1646  */
    break;

  case 184:
#line 905 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(PI_, ENULL, ENULL); }
#line 3928 "y.tab.c" /* yacc.c:1646  */
    break;

  case 185:
#line 906 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new_str((yyvsp[0].sval)); }
#line 3934 "y.tab.c" /* yacc.c:1646  */
    break;

  case 186:
#line 907 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new('!', (yyvsp[0].enode), ENULL); }
#line 3940 "y.tab.c" /* yacc.c:1646  */
    break;

  case 187:
#line 908 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new('!', (yyvsp[0].enode), ENULL); }
#line 3946 "y.tab.c" /* yacc.c:1646  */
    break;

  case 188:
#line 910 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(FILENAME, (yyvsp[-1].enode), ENULL); }
#line 3952 "y.tab.c" /* yacc.c:1646  */
    break;

  case 189:
#line 911 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(MYROW, ENULL, ENULL);}
#line 3958 "y.tab.c" /* yacc.c:1646  */
    break;

  case 190:
#line 912 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(MYCOL, ENULL, ENULL);}
#line 3964 "y.tab.c" /* yacc.c:1646  */
    break;

  case 191:
#line 913 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(LASTROW, ENULL, ENULL);}
#line 3970 "y.tab.c" /* yacc.c:1646  */
    break;

  case 192:
#line 914 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(LASTCOL, ENULL, ENULL);}
#line 3976 "y.tab.c" /* yacc.c:1646  */
    break;

  case 193:
#line 915 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(COLTOA, (yyvsp[-1].enode), ENULL);}
#line 3982 "y.tab.c" /* yacc.c:1646  */
    break;

  case 194:
#line 916 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(ASCII, (yyvsp[-1].enode), ENULL); }
#line 3988 "y.tab.c" /* yacc.c:1646  */
    break;

  case 195:
#line 917 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(SET8BIT, (yyvsp[-1].enode), ENULL); }
#line 3994 "y.tab.c" /* yacc.c:1646  */
    break;

  case 196:
#line 918 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(CHR, (yyvsp[-1].enode), ENULL);}
#line 4000 "y.tab.c" /* yacc.c:1646  */
    break;

  case 197:
#line 919 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(ERR_, ENULL, ENULL); }
#line 4006 "y.tab.c" /* yacc.c:1646  */
    break;

  case 198:
#line 920 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(ERR_, ENULL, ENULL); }
#line 4012 "y.tab.c" /* yacc.c:1646  */
    break;

  case 199:
#line 921 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(REF_, ENULL, ENULL); }
#line 4018 "y.tab.c" /* yacc.c:1646  */
    break;

  case 200:
#line 922 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(REF_, ENULL, ENULL); }
#line 4024 "y.tab.c" /* yacc.c:1646  */
    break;

  case 201:
#line 937 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new('+', (yyvsp[-2].enode), (yyvsp[0].enode)); }
#line 4030 "y.tab.c" /* yacc.c:1646  */
    break;

  case 202:
#line 938 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new('-', (yyvsp[-2].enode), (yyvsp[0].enode)); }
#line 4036 "y.tab.c" /* yacc.c:1646  */
    break;

  case 203:
#line 939 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new('*', (yyvsp[-2].enode), (yyvsp[0].enode)); }
#line 4042 "y.tab.c" /* yacc.c:1646  */
    break;

  case 204:
#line 940 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new('/', (yyvsp[-2].enode), (yyvsp[0].enode)); }
#line 4048 "y.tab.c" /* yacc.c:1646  */
    break;

  case 205:
#line 941 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new('%', (yyvsp[-2].enode), (yyvsp[0].enode)); }
#line 4054 "y.tab.c" /* yacc.c:1646  */
    break;

  case 206:
#line 942 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new('m', (yyvsp[0].enode), ENULL); }
#line 4060 "y.tab.c" /* yacc.c:1646  */
    break;

  case 207:
#line 943 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new('^', (yyvsp[-2].enode), (yyvsp[0].enode)); }
#line 4066 "y.tab.c" /* yacc.c:1646  */
    break;

  case 209:
#line 945 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new('?', (yyvsp[-4].enode), new(':', (yyvsp[-2].enode), (yyvsp[0].enode))); }
#line 4072 "y.tab.c" /* yacc.c:1646  */
    break;

  case 210:
#line 946 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(';', (yyvsp[-2].enode), (yyvsp[0].enode)); }
#line 4078 "y.tab.c" /* yacc.c:1646  */
    break;

  case 211:
#line 947 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new('<', (yyvsp[-2].enode), (yyvsp[0].enode)); }
#line 4084 "y.tab.c" /* yacc.c:1646  */
    break;

  case 212:
#line 948 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new('=', (yyvsp[-2].enode), (yyvsp[0].enode)); }
#line 4090 "y.tab.c" /* yacc.c:1646  */
    break;

  case 213:
#line 949 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new('>', (yyvsp[-2].enode), (yyvsp[0].enode)); }
#line 4096 "y.tab.c" /* yacc.c:1646  */
    break;

  case 214:
#line 950 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new('&', (yyvsp[-2].enode), (yyvsp[0].enode)); }
#line 4102 "y.tab.c" /* yacc.c:1646  */
    break;

  case 215:
#line 951 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new('|', (yyvsp[-2].enode), (yyvsp[0].enode)); }
#line 4108 "y.tab.c" /* yacc.c:1646  */
    break;

  case 216:
#line 952 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new('!', new('>', (yyvsp[-3].enode), (yyvsp[0].enode)), ENULL); }
#line 4114 "y.tab.c" /* yacc.c:1646  */
    break;

  case 217:
#line 953 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new('!', new('=', (yyvsp[-3].enode), (yyvsp[0].enode)), ENULL); }
#line 4120 "y.tab.c" /* yacc.c:1646  */
    break;

  case 218:
#line 954 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new('!', new('=', (yyvsp[-3].enode), (yyvsp[0].enode)), ENULL); }
#line 4126 "y.tab.c" /* yacc.c:1646  */
    break;

  case 219:
#line 955 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new('!', new('<', (yyvsp[-3].enode), (yyvsp[0].enode)), ENULL); }
#line 4132 "y.tab.c" /* yacc.c:1646  */
    break;

  case 220:
#line 956 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new('#', (yyvsp[-2].enode), (yyvsp[0].enode)); }
#line 4138 "y.tab.c" /* yacc.c:1646  */
    break;

  case 221:
#line 959 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(ELIST, ENULL, (yyvsp[0].enode)); }
#line 4144 "y.tab.c" /* yacc.c:1646  */
    break;

  case 222:
#line 960 "gram.y" /* yacc.c:1646  */
    { (yyval.enode) = new(ELIST, (yyvsp[-2].enode), (yyvsp[0].enode)); }
#line 4150 "y.tab.c" /* yacc.c:1646  */
    break;

  case 223:
#line 963 "gram.y" /* yacc.c:1646  */
    { (yyval.rval).left = (yyvsp[-2].ent); (yyval.rval).right = (yyvsp[0].ent); }
#line 4156 "y.tab.c" /* yacc.c:1646  */
    break;

  case 224:
#line 964 "gram.y" /* yacc.c:1646  */
    { (yyval.rval) = (yyvsp[0].rval); }
#line 4162 "y.tab.c" /* yacc.c:1646  */
    break;

  case 225:
#line 967 "gram.y" /* yacc.c:1646  */
    { (yyval.ent).vp = lookat((yyvsp[0].ival), (yyvsp[-1].ival));
                                    (yyval.ent).vf = 0;
                                  }
#line 4170 "y.tab.c" /* yacc.c:1646  */
    break;

  case 226:
#line 970 "gram.y" /* yacc.c:1646  */
    { (yyval.ent).vp = lookat((yyvsp[0].ival), (yyvsp[-1].ival));
                                    (yyval.ent).vf = FIX_COL;
                                  }
#line 4178 "y.tab.c" /* yacc.c:1646  */
    break;

  case 227:
#line 973 "gram.y" /* yacc.c:1646  */
    { (yyval.ent).vp = lookat((yyvsp[0].ival), (yyvsp[-2].ival));
                                    (yyval.ent).vf = FIX_ROW;
                                  }
#line 4186 "y.tab.c" /* yacc.c:1646  */
    break;

  case 228:
#line 976 "gram.y" /* yacc.c:1646  */
    {
                                    (yyval.ent).vp = lookat((yyvsp[0].ival), (yyvsp[-2].ival));
                                    (yyval.ent).vf = FIX_ROW | FIX_COL;
                                  }
#line 4195 "y.tab.c" /* yacc.c:1646  */
    break;

  case 229:
#line 980 "gram.y" /* yacc.c:1646  */
    {
                                    (yyval.ent) = (yyvsp[0].rval).left;
                                  }
#line 4203 "y.tab.c" /* yacc.c:1646  */
    break;

  case 230:
#line 985 "gram.y" /* yacc.c:1646  */
    { (yyval.rval) = (yyvsp[0].rval); }
#line 4209 "y.tab.c" /* yacc.c:1646  */
    break;

  case 231:
#line 986 "gram.y" /* yacc.c:1646  */
    { (yyval.rval).left = (yyvsp[0].ent); (yyval.rval).right = (yyvsp[0].ent); }
#line 4215 "y.tab.c" /* yacc.c:1646  */
    break;

  case 232:
#line 989 "gram.y" /* yacc.c:1646  */
    { (yyval.fval) = (double) (yyvsp[0].ival); }
#line 4221 "y.tab.c" /* yacc.c:1646  */
    break;

  case 233:
#line 990 "gram.y" /* yacc.c:1646  */
    { (yyval.fval) = (yyvsp[0].fval); }
#line 4227 "y.tab.c" /* yacc.c:1646  */
    break;

  case 234:
#line 991 "gram.y" /* yacc.c:1646  */
    { (yyval.fval) = -(yyvsp[0].fval); }
#line 4233 "y.tab.c" /* yacc.c:1646  */
    break;

  case 235:
#line 992 "gram.y" /* yacc.c:1646  */
    { (yyval.fval) = (yyvsp[0].fval); }
#line 4239 "y.tab.c" /* yacc.c:1646  */
    break;

  case 236:
#line 995 "gram.y" /* yacc.c:1646  */
    { (yyval.sval) = (yyvsp[0].sval); }
#line 4245 "y.tab.c" /* yacc.c:1646  */
    break;

  case 237:
#line 996 "gram.y" /* yacc.c:1646  */
    {
                                    char *s, *s1;
                                    s1 = (yyvsp[0].ent).vp->label;
                                    if (!s1)
                                    s1 = "NULL_STRING";
                                    s = scxmalloc((unsigned)strlen(s1)+1);
                                    (void) strcpy(s, s1);
                                    (yyval.sval) = s;
                                  }
#line 4259 "y.tab.c" /* yacc.c:1646  */
    break;

  case 240:
#line 1014 "gram.y" /* yacc.c:1646  */
    {  if ((yyvsp[0].ival) == 0) parse_str(user_conf_d, "overlap=0");
                                     else         parse_str(user_conf_d, "overlap=1"); }
#line 4266 "y.tab.c" /* yacc.c:1646  */
    break;

  case 241:
#line 1016 "gram.y" /* yacc.c:1646  */
    {               parse_str(user_conf_d, "overlap=1"); }
#line 4272 "y.tab.c" /* yacc.c:1646  */
    break;

  case 242:
#line 1017 "gram.y" /* yacc.c:1646  */
    {               parse_str(user_conf_d, "overlap=0"); }
#line 4278 "y.tab.c" /* yacc.c:1646  */
    break;

  case 243:
#line 1018 "gram.y" /* yacc.c:1646  */
    {
                                                  char cmd[MAXCMD];
                                                  sprintf(cmd, "autobackup=%d", (yyvsp[0].ival));
                                                  parse_str(user_conf_d, cmd); }
#line 4287 "y.tab.c" /* yacc.c:1646  */
    break;

  case 244:
#line 1022 "gram.y" /* yacc.c:1646  */
    {               parse_str(user_conf_d, "autobackup=0"); }
#line 4293 "y.tab.c" /* yacc.c:1646  */
    break;

  case 245:
#line 1023 "gram.y" /* yacc.c:1646  */
    {               parse_str(user_conf_d, "autocalc=1"); }
#line 4299 "y.tab.c" /* yacc.c:1646  */
    break;

  case 246:
#line 1024 "gram.y" /* yacc.c:1646  */
    {  if ((yyvsp[0].ival) == 0) parse_str(user_conf_d, "autocalc=0");
                                     else         parse_str(user_conf_d, "autocalc=1"); }
#line 4306 "y.tab.c" /* yacc.c:1646  */
    break;

  case 247:
#line 1026 "gram.y" /* yacc.c:1646  */
    {               parse_str(user_conf_d, "autocalc=0"); }
#line 4312 "y.tab.c" /* yacc.c:1646  */
    break;

  case 248:
#line 1027 "gram.y" /* yacc.c:1646  */
    {               parse_str(user_conf_d, "debug=1"); }
#line 4318 "y.tab.c" /* yacc.c:1646  */
    break;

  case 249:
#line 1028 "gram.y" /* yacc.c:1646  */
    {  if ((yyvsp[0].ival) == 0) parse_str(user_conf_d, "debug=0");
                                     else         parse_str(user_conf_d, "debug=1"); }
#line 4325 "y.tab.c" /* yacc.c:1646  */
    break;

  case 250:
#line 1030 "gram.y" /* yacc.c:1646  */
    {               parse_str(user_conf_d, "debug=0"); }
#line 4331 "y.tab.c" /* yacc.c:1646  */
    break;

  case 251:
#line 1031 "gram.y" /* yacc.c:1646  */
    {               parse_str(user_conf_d, "trigger=1"); }
#line 4337 "y.tab.c" /* yacc.c:1646  */
    break;

  case 252:
#line 1032 "gram.y" /* yacc.c:1646  */
    {  if ((yyvsp[0].ival) == 0) parse_str(user_conf_d, "trigger=0");
                                     else         parse_str(user_conf_d, "trigger=1"); }
#line 4344 "y.tab.c" /* yacc.c:1646  */
    break;

  case 253:
#line 1034 "gram.y" /* yacc.c:1646  */
    {               parse_str(user_conf_d, "trigger=0"); }
#line 4350 "y.tab.c" /* yacc.c:1646  */
    break;

  case 254:
#line 1035 "gram.y" /* yacc.c:1646  */
    {               parse_str(user_conf_d, "external_functions=1"); }
#line 4356 "y.tab.c" /* yacc.c:1646  */
    break;

  case 255:
#line 1036 "gram.y" /* yacc.c:1646  */
    {
                                     if ((yyvsp[0].ival) == 0) parse_str(user_conf_d, "external_functions=0");
                                     else         parse_str(user_conf_d, "external_functions=1"); }
#line 4364 "y.tab.c" /* yacc.c:1646  */
    break;

  case 256:
#line 1039 "gram.y" /* yacc.c:1646  */
    {               parse_str(user_conf_d, "external_functions=0"); }
#line 4370 "y.tab.c" /* yacc.c:1646  */
    break;

  case 257:
#line 1040 "gram.y" /* yacc.c:1646  */
    {               parse_str(user_conf_d, "half_page_scroll=1"); }
#line 4376 "y.tab.c" /* yacc.c:1646  */
    break;

  case 258:
#line 1042 "gram.y" /* yacc.c:1646  */
    {  if ((yyvsp[0].ival) == 0) parse_str(user_conf_d, "half_page_scroll=0");
                                     else         parse_str(user_conf_d, "half_page_scroll=1"); }
#line 4383 "y.tab.c" /* yacc.c:1646  */
    break;

  case 259:
#line 1044 "gram.y" /* yacc.c:1646  */
    {               parse_str(user_conf_d, "half_page_scroll=0"); }
#line 4389 "y.tab.c" /* yacc.c:1646  */
    break;

  case 260:
#line 1045 "gram.y" /* yacc.c:1646  */
    {               parse_str(user_conf_d, "quit_afterload=1"); }
#line 4395 "y.tab.c" /* yacc.c:1646  */
    break;

  case 261:
#line 1047 "gram.y" /* yacc.c:1646  */
    {  if ((yyvsp[0].ival) == 0) parse_str(user_conf_d, "quit_afterload=0");
                                     else         parse_str(user_conf_d, "quit_afterload=1"); }
#line 4402 "y.tab.c" /* yacc.c:1646  */
    break;

  case 262:
#line 1049 "gram.y" /* yacc.c:1646  */
    {               parse_str(user_conf_d, "quit_afterload=0"); }
#line 4408 "y.tab.c" /* yacc.c:1646  */
    break;

  case 263:
#line 1050 "gram.y" /* yacc.c:1646  */
    {               parse_str(user_conf_d, "xlsx_readformulas=1"); }
#line 4414 "y.tab.c" /* yacc.c:1646  */
    break;

  case 264:
#line 1052 "gram.y" /* yacc.c:1646  */
    {  if ((yyvsp[0].ival) == 0) parse_str(user_conf_d, "xlsx_readformulas=0");
                                     else         parse_str(user_conf_d, "xlsx_readformulas=1"); }
#line 4421 "y.tab.c" /* yacc.c:1646  */
    break;

  case 265:
#line 1054 "gram.y" /* yacc.c:1646  */
    {               parse_str(user_conf_d, "xlsx_readformulas=0"); }
#line 4427 "y.tab.c" /* yacc.c:1646  */
    break;

  case 266:
#line 1055 "gram.y" /* yacc.c:1646  */
    {               parse_str(user_conf_d, "nocurses=1"); }
#line 4433 "y.tab.c" /* yacc.c:1646  */
    break;

  case 267:
#line 1056 "gram.y" /* yacc.c:1646  */
    {  if ((yyvsp[0].ival) == 0) parse_str(user_conf_d, "nocurses=0");
                                     else         parse_str(user_conf_d, "nocurses=1"); }
#line 4440 "y.tab.c" /* yacc.c:1646  */
    break;

  case 268:
#line 1058 "gram.y" /* yacc.c:1646  */
    {               parse_str(user_conf_d, "nocurses=0"); }
#line 4446 "y.tab.c" /* yacc.c:1646  */
    break;

  case 269:
#line 1059 "gram.y" /* yacc.c:1646  */
    {               parse_str(user_conf_d, "numeric=1"); }
#line 4452 "y.tab.c" /* yacc.c:1646  */
    break;

  case 270:
#line 1060 "gram.y" /* yacc.c:1646  */
    {  if ((yyvsp[0].ival) == 0) parse_str(user_conf_d, "numeric=0");
                                     else         parse_str(user_conf_d, "numeric=1"); }
#line 4459 "y.tab.c" /* yacc.c:1646  */
    break;

  case 271:
#line 1062 "gram.y" /* yacc.c:1646  */
    {               parse_str(user_conf_d, "numeric=0"); }
#line 4465 "y.tab.c" /* yacc.c:1646  */
    break;

  case 272:
#line 1063 "gram.y" /* yacc.c:1646  */
    {               parse_str(user_conf_d, "ignorecase=1"); }
#line 4471 "y.tab.c" /* yacc.c:1646  */
    break;

  case 273:
#line 1064 "gram.y" /* yacc.c:1646  */
    {  if ((yyvsp[0].ival) == 0) parse_str(user_conf_d, "ignorecase=0");
                                     else         parse_str(user_conf_d, "ignorecase=1"); }
#line 4478 "y.tab.c" /* yacc.c:1646  */
    break;

  case 274:
#line 1066 "gram.y" /* yacc.c:1646  */
    {               parse_str(user_conf_d, "ignorecase=0"); }
#line 4484 "y.tab.c" /* yacc.c:1646  */
    break;

  case 275:
#line 1067 "gram.y" /* yacc.c:1646  */
    {               parse_str(user_conf_d, "numeric_decimal=1"); }
#line 4490 "y.tab.c" /* yacc.c:1646  */
    break;

  case 276:
#line 1069 "gram.y" /* yacc.c:1646  */
    {  if ((yyvsp[0].ival) == 0) parse_str(user_conf_d, "numeric_decimal=0");
                                     else         parse_str(user_conf_d, "numeric_decimal=1"); }
#line 4497 "y.tab.c" /* yacc.c:1646  */
    break;

  case 277:
#line 1071 "gram.y" /* yacc.c:1646  */
    {               parse_str(user_conf_d, "numeric_decimal=0"); }
#line 4503 "y.tab.c" /* yacc.c:1646  */
    break;

  case 278:
#line 1072 "gram.y" /* yacc.c:1646  */
    {               parse_str(user_conf_d, "numeric_zero=1"); }
#line 4509 "y.tab.c" /* yacc.c:1646  */
    break;

  case 279:
#line 1074 "gram.y" /* yacc.c:1646  */
    {  if ((yyvsp[0].ival) == 0) parse_str(user_conf_d, "numeric_zero=0");
                                     else         parse_str(user_conf_d, "numeric_zero=1"); }
#line 4516 "y.tab.c" /* yacc.c:1646  */
    break;

  case 280:
#line 1076 "gram.y" /* yacc.c:1646  */
    {               parse_str(user_conf_d, "numeric_zero=0"); }
#line 4522 "y.tab.c" /* yacc.c:1646  */
    break;

  case 281:
#line 1077 "gram.y" /* yacc.c:1646  */
    {               parse_str(user_conf_d, "newline_action=0"); }
#line 4528 "y.tab.c" /* yacc.c:1646  */
    break;

  case 282:
#line 1078 "gram.y" /* yacc.c:1646  */
    {
                                  char * s = (char *) (yyvsp[0].sval);
                                  if (s[0] =='j') parse_str(user_conf_d, "newline_action=j");
                                  else if (s[0] =='l')
                                  parse_str(user_conf_d, "newline_action=l");
                                  }
#line 4539 "y.tab.c" /* yacc.c:1646  */
    break;

  case 283:
#line 1084 "gram.y" /* yacc.c:1646  */
    {
                                  char cmd[MAXCMD];
                                  char * s = (char *) (yyvsp[0].sval);
                                  sprintf(cmd, "default_copy_to_clipboard_cmd=%s", s);
                                  parse_str(user_conf_d, cmd);
                                  scxfree(s);
                                  }
#line 4551 "y.tab.c" /* yacc.c:1646  */
    break;

  case 284:
#line 1091 "gram.y" /* yacc.c:1646  */
    {
                                  char cmd[MAXCMD];
                                  char * s = (char *) (yyvsp[0].sval);
                                  sprintf(cmd, "default_paste_from_clipboard_cmd=%s", s);
                                  parse_str(user_conf_d, cmd);
                                  scxfree(s);
                                  }
#line 4563 "y.tab.c" /* yacc.c:1646  */
    break;

  case 285:
#line 1099 "gram.y" /* yacc.c:1646  */
    {      parse_str(user_conf_d, "copy_to_clipboard_delimited_tab=1"); }
#line 4569 "y.tab.c" /* yacc.c:1646  */
    break;

  case 286:
#line 1102 "gram.y" /* yacc.c:1646  */
    {  if ((yyvsp[0].ival) == 0) parse_str(user_conf_d, "copy_to_clipboard_delimited_tab=0");
                                     else         parse_str(user_conf_d, "copy_to_clipboard_delimited_tab=1"); }
#line 4576 "y.tab.c" /* yacc.c:1646  */
    break;

  case 287:
#line 1104 "gram.y" /* yacc.c:1646  */
    {    parse_str(user_conf_d, "copy_to_clipboard_delimited_tab=0"); }
#line 4582 "y.tab.c" /* yacc.c:1646  */
    break;

  case 288:
#line 1106 "gram.y" /* yacc.c:1646  */
    {
                                     if ((yyvsp[0].ival) == 0) parse_str(user_conf_d, "newline_action=0"); }
#line 4589 "y.tab.c" /* yacc.c:1646  */
    break;

  case 289:
#line 1108 "gram.y" /* yacc.c:1646  */
    {               parse_str(user_conf_d, "tm_gmtoff=-10800"); }
#line 4595 "y.tab.c" /* yacc.c:1646  */
    break;

  case 290:
#line 1109 "gram.y" /* yacc.c:1646  */
    {
                                     char * s = scxmalloc((unsigned) BUFFERSIZE);
                                     sprintf(s, "tm_gmtoff=%d", (int) (yyvsp[0].fval));
                                     parse_str(user_conf_d, s);
                                     scxfree(s);
                                  }
#line 4606 "y.tab.c" /* yacc.c:1646  */
    break;


#line 4610 "y.tab.c" /* yacc.c:1646  */
      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYEMPTY : YYTRANSLATE (yychar);

  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
# define YYSYNTAX_ERROR yysyntax_error (&yymsg_alloc, &yymsg, \
                                        yyssp, yytoken)
      {
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        yysyntax_error_status = YYSYNTAX_ERROR;
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == 1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = (char *) YYSTACK_ALLOC (yymsg_alloc);
            if (!yymsg)
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = 2;
              }
            else
              {
                yysyntax_error_status = YYSYNTAX_ERROR;
                yymsgp = yymsg;
              }
          }
        yyerror (yymsgp);
        if (yysyntax_error_status == 2)
          goto yyexhaustedlab;
      }
# undef YYSYNTAX_ERROR
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYTERROR;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;


      yydestruct ("Error: popping",
                  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#if !defined yyoverflow || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  yystos[*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  return yyresult;
}
