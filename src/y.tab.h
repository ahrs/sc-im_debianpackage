/* A Bison parser, made by GNU Bison 3.0.4.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

#ifndef YY_YY_Y_TAB_H_INCLUDED
# define YY_YY_Y_TAB_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    STRING = 258,
    COL = 259,
    NUMBER = 260,
    FNUMBER = 261,
    RANGE = 262,
    VAR = 263,
    WORD = 264,
    MAPWORD = 265,
    PLUGIN = 266,
    S_SHOW = 267,
    S_HIDE = 268,
    S_SHOWROW = 269,
    S_HIDEROW = 270,
    S_SHOWCOL = 271,
    S_HIDECOL = 272,
    S_FREEZE = 273,
    S_UNFREEZE = 274,
    S_MARK = 275,
    S_AUTOJUS = 276,
    S_PAD = 277,
    S_DATEFMT = 278,
    S_SUBTOTAL = 279,
    S_RSUBTOTAL = 280,
    S_FORMAT = 281,
    S_FMT = 282,
    S_LET = 283,
    S_LABEL = 284,
    S_LEFTSTRING = 285,
    S_RIGHTSTRING = 286,
    S_LEFTJUSTIFY = 287,
    S_RIGHTJUSTIFY = 288,
    S_CENTER = 289,
    S_SORT = 290,
    S_FILTERON = 291,
    S_GOTO = 292,
    S_CCOPY = 293,
    S_CPASTE = 294,
    S_PLOT = 295,
    S_LOCK = 296,
    S_UNLOCK = 297,
    S_DEFINE = 298,
    S_UNDEFINE = 299,
    S_DETAIL = 300,
    S_EVAL = 301,
    S_SEVAL = 302,
    S_ERROR = 303,
    S_FILL = 304,
    S_SHIFT = 305,
    S_GETNUM = 306,
    S_GETSTRING = 307,
    S_GETEXP = 308,
    S_GETFMT = 309,
    S_GETFORMAT = 310,
    S_RECALC = 311,
    S_QUIT = 312,
    S_REBUILD_GRAPH = 313,
    S_PRINT_GRAPH = 314,
    S_SYNCREFS = 315,
    S_REDO = 316,
    S_UNDO = 317,
    S_IMAP = 318,
    S_NMAP = 319,
    S_INOREMAP = 320,
    S_NNOREMAP = 321,
    S_NUNMAP = 322,
    S_IUNMAP = 323,
    S_COLOR = 324,
    S_CELLCOLOR = 325,
    S_UNFORMAT = 326,
    S_REDEFINE_COLOR = 327,
    S_SET = 328,
    S_FCOPY = 329,
    S_FSUM = 330,
    S_TRIGGER = 331,
    S_UNTRIGGER = 332,
    K_AUTOBACKUP = 333,
    K_NOAUTOBACKUP = 334,
    K_AUTOCALC = 335,
    K_NOAUTOCALC = 336,
    K_DEBUG = 337,
    K_NODEBUG = 338,
    K_TRG = 339,
    K_NOTRG = 340,
    K_EXTERNAL_FUNCTIONS = 341,
    K_NOEXTERNAL_FUNCTIONS = 342,
    K_HALF_PAGE_SCROLL = 343,
    K_NOHALF_PAGE_SCROLL = 344,
    K_NOCURSES = 345,
    K_CURSES = 346,
    K_NUMERIC = 347,
    K_NONUMERIC = 348,
    K_NUMERIC_DECIMAL = 349,
    K_NONUMERIC_DECIMAL = 350,
    K_NUMERIC_ZERO = 351,
    K_NONUMERIC_ZERO = 352,
    K_OVERLAP = 353,
    K_NOOVERLAP = 354,
    K_QUIT_AFTERLOAD = 355,
    K_NOQUIT_AFTERLOAD = 356,
    K_XLSX_READFORMULAS = 357,
    K_NOXLSX_READFORMULAS = 358,
    K_DEFAULT_COPY_TO_CLIPBOARD_CMD = 359,
    K_DEFAULT_PASTE_FROM_CLIPBOARD_CMD = 360,
    K_COPY_TO_CLIPBOARD_DELIMITED_TAB = 361,
    K_NOCOPY_TO_CLIPBOARD_DELIMITED_TAB = 362,
    K_IGNORECASE = 363,
    K_NOIGNORECASE = 364,
    K_TM_GMTOFF = 365,
    K_NEWLINE_ACTION = 366,
    K_ERROR = 367,
    K_INVALID = 368,
    K_FIXED = 369,
    K_SUM = 370,
    K_PROD = 371,
    K_AVG = 372,
    K_STDDEV = 373,
    K_COUNT = 374,
    K_ROWS = 375,
    K_COLS = 376,
    K_ABS = 377,
    K_FROW = 378,
    K_FCOL = 379,
    K_ACOS = 380,
    K_ASIN = 381,
    K_ATAN = 382,
    K_ATAN2 = 383,
    K_CEIL = 384,
    K_COS = 385,
    K_EXP = 386,
    K_FABS = 387,
    K_FLOOR = 388,
    K_HYPOT = 389,
    K_LN = 390,
    K_LOG = 391,
    K_PI = 392,
    K_POW = 393,
    K_SIN = 394,
    K_SQRT = 395,
    K_TAN = 396,
    K_DTR = 397,
    K_RTD = 398,
    K_MAX = 399,
    K_MIN = 400,
    K_RND = 401,
    K_ROUND = 402,
    K_IF = 403,
    K_PV = 404,
    K_FV = 405,
    K_PMT = 406,
    K_HOUR = 407,
    K_MINUTE = 408,
    K_SECOND = 409,
    K_MONTH = 410,
    K_DAY = 411,
    K_YEAR = 412,
    K_NOW = 413,
    K_DATE = 414,
    K_DTS = 415,
    K_TTS = 416,
    K_FMT = 417,
    K_REPLACE = 418,
    K_SUBSTR = 419,
    K_UPPER = 420,
    K_LOWER = 421,
    K_CAPITAL = 422,
    K_STON = 423,
    K_SLEN = 424,
    K_EQS = 425,
    K_EXT = 426,
    K_LUA = 427,
    K_NVAL = 428,
    K_SVAL = 429,
    K_LOOKUP = 430,
    K_HLOOKUP = 431,
    K_VLOOKUP = 432,
    K_INDEX = 433,
    K_STINDEX = 434,
    K_TBLSTYLE = 435,
    K_TBL = 436,
    K_LATEX = 437,
    K_SLATEX = 438,
    K_TEX = 439,
    K_FRAME = 440,
    K_RNDTOEVEN = 441,
    K_FILENAME = 442,
    K_MYROW = 443,
    K_MYCOL = 444,
    K_LASTROW = 445,
    K_LASTCOL = 446,
    K_COLTOA = 447,
    K_CRACTION = 448,
    K_CRROW = 449,
    K_CRCOL = 450,
    K_ROWLIMIT = 451,
    K_COLLIMIT = 452,
    K_PAGESIZE = 453,
    K_ERR = 454,
    K_REF = 455,
    K_LOCALE = 456,
    K_SET8BIT = 457,
    K_ASCII = 458,
    K_CHR = 459
  };
#endif
/* Tokens.  */
#define STRING 258
#define COL 259
#define NUMBER 260
#define FNUMBER 261
#define RANGE 262
#define VAR 263
#define WORD 264
#define MAPWORD 265
#define PLUGIN 266
#define S_SHOW 267
#define S_HIDE 268
#define S_SHOWROW 269
#define S_HIDEROW 270
#define S_SHOWCOL 271
#define S_HIDECOL 272
#define S_FREEZE 273
#define S_UNFREEZE 274
#define S_MARK 275
#define S_AUTOJUS 276
#define S_PAD 277
#define S_DATEFMT 278
#define S_SUBTOTAL 279
#define S_RSUBTOTAL 280
#define S_FORMAT 281
#define S_FMT 282
#define S_LET 283
#define S_LABEL 284
#define S_LEFTSTRING 285
#define S_RIGHTSTRING 286
#define S_LEFTJUSTIFY 287
#define S_RIGHTJUSTIFY 288
#define S_CENTER 289
#define S_SORT 290
#define S_FILTERON 291
#define S_GOTO 292
#define S_CCOPY 293
#define S_CPASTE 294
#define S_PLOT 295
#define S_LOCK 296
#define S_UNLOCK 297
#define S_DEFINE 298
#define S_UNDEFINE 299
#define S_DETAIL 300
#define S_EVAL 301
#define S_SEVAL 302
#define S_ERROR 303
#define S_FILL 304
#define S_SHIFT 305
#define S_GETNUM 306
#define S_GETSTRING 307
#define S_GETEXP 308
#define S_GETFMT 309
#define S_GETFORMAT 310
#define S_RECALC 311
#define S_QUIT 312
#define S_REBUILD_GRAPH 313
#define S_PRINT_GRAPH 314
#define S_SYNCREFS 315
#define S_REDO 316
#define S_UNDO 317
#define S_IMAP 318
#define S_NMAP 319
#define S_INOREMAP 320
#define S_NNOREMAP 321
#define S_NUNMAP 322
#define S_IUNMAP 323
#define S_COLOR 324
#define S_CELLCOLOR 325
#define S_UNFORMAT 326
#define S_REDEFINE_COLOR 327
#define S_SET 328
#define S_FCOPY 329
#define S_FSUM 330
#define S_TRIGGER 331
#define S_UNTRIGGER 332
#define K_AUTOBACKUP 333
#define K_NOAUTOBACKUP 334
#define K_AUTOCALC 335
#define K_NOAUTOCALC 336
#define K_DEBUG 337
#define K_NODEBUG 338
#define K_TRG 339
#define K_NOTRG 340
#define K_EXTERNAL_FUNCTIONS 341
#define K_NOEXTERNAL_FUNCTIONS 342
#define K_HALF_PAGE_SCROLL 343
#define K_NOHALF_PAGE_SCROLL 344
#define K_NOCURSES 345
#define K_CURSES 346
#define K_NUMERIC 347
#define K_NONUMERIC 348
#define K_NUMERIC_DECIMAL 349
#define K_NONUMERIC_DECIMAL 350
#define K_NUMERIC_ZERO 351
#define K_NONUMERIC_ZERO 352
#define K_OVERLAP 353
#define K_NOOVERLAP 354
#define K_QUIT_AFTERLOAD 355
#define K_NOQUIT_AFTERLOAD 356
#define K_XLSX_READFORMULAS 357
#define K_NOXLSX_READFORMULAS 358
#define K_DEFAULT_COPY_TO_CLIPBOARD_CMD 359
#define K_DEFAULT_PASTE_FROM_CLIPBOARD_CMD 360
#define K_COPY_TO_CLIPBOARD_DELIMITED_TAB 361
#define K_NOCOPY_TO_CLIPBOARD_DELIMITED_TAB 362
#define K_IGNORECASE 363
#define K_NOIGNORECASE 364
#define K_TM_GMTOFF 365
#define K_NEWLINE_ACTION 366
#define K_ERROR 367
#define K_INVALID 368
#define K_FIXED 369
#define K_SUM 370
#define K_PROD 371
#define K_AVG 372
#define K_STDDEV 373
#define K_COUNT 374
#define K_ROWS 375
#define K_COLS 376
#define K_ABS 377
#define K_FROW 378
#define K_FCOL 379
#define K_ACOS 380
#define K_ASIN 381
#define K_ATAN 382
#define K_ATAN2 383
#define K_CEIL 384
#define K_COS 385
#define K_EXP 386
#define K_FABS 387
#define K_FLOOR 388
#define K_HYPOT 389
#define K_LN 390
#define K_LOG 391
#define K_PI 392
#define K_POW 393
#define K_SIN 394
#define K_SQRT 395
#define K_TAN 396
#define K_DTR 397
#define K_RTD 398
#define K_MAX 399
#define K_MIN 400
#define K_RND 401
#define K_ROUND 402
#define K_IF 403
#define K_PV 404
#define K_FV 405
#define K_PMT 406
#define K_HOUR 407
#define K_MINUTE 408
#define K_SECOND 409
#define K_MONTH 410
#define K_DAY 411
#define K_YEAR 412
#define K_NOW 413
#define K_DATE 414
#define K_DTS 415
#define K_TTS 416
#define K_FMT 417
#define K_REPLACE 418
#define K_SUBSTR 419
#define K_UPPER 420
#define K_LOWER 421
#define K_CAPITAL 422
#define K_STON 423
#define K_SLEN 424
#define K_EQS 425
#define K_EXT 426
#define K_LUA 427
#define K_NVAL 428
#define K_SVAL 429
#define K_LOOKUP 430
#define K_HLOOKUP 431
#define K_VLOOKUP 432
#define K_INDEX 433
#define K_STINDEX 434
#define K_TBLSTYLE 435
#define K_TBL 436
#define K_LATEX 437
#define K_SLATEX 438
#define K_TEX 439
#define K_FRAME 440
#define K_RNDTOEVEN 441
#define K_FILENAME 442
#define K_MYROW 443
#define K_MYCOL 444
#define K_LASTROW 445
#define K_LASTCOL 446
#define K_COLTOA 447
#define K_CRACTION 448
#define K_CRROW 449
#define K_CRCOL 450
#define K_ROWLIMIT 451
#define K_COLLIMIT 452
#define K_PAGESIZE 453
#define K_ERR 454
#define K_REF 455
#define K_LOCALE 456
#define K_SET8BIT 457
#define K_ASCII 458
#define K_CHR 459

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED

union YYSTYPE
{
#line 43 "gram.y" /* yacc.c:1909  */

    int ival;
    double fval;
    struct ent_ptr ent;
    struct enode * enode;
    char * sval;
    struct range_s rval;

#line 471 "y.tab.h" /* yacc.c:1909  */
};

typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);

#endif /* !YY_YY_Y_TAB_H_INCLUDED  */
